package net.woodpixel.essentials.configuration;

import net.woodpixel.essentials.Data;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class TptoggleConfiguration {

    private JavaPlugin javaPlugin;
    private static TptoggleConfiguration configuration;

    private File file;
    private YamlConfiguration config;

    public void setConfiguration() {
        configuration = this;
    }

    public TptoggleConfiguration(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        file = new File(this.javaPlugin.getDataFolder().getPath(), "teleportConfig.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {e.printStackTrace();}
        }
        config = YamlConfiguration.loadConfiguration(file);
    }

    public static TptoggleConfiguration getConfiguration() {
        return configuration;
    }

    public void toggle(Player p) {

        if (config.getString(p.getUniqueId().toString()) == null) {
            addToggle(p);
            return;
        }

        removeToggle(p);

    }

    private void addToggle(Player p) {

        config.set(p.getUniqueId().toString(), "toggled");
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        p.sendMessage(Data.getData().prefix + "§6Du erhälst nun keine Anfragen mehr!");

    }

    private void removeToggle(Player p) {

        config.set(p.getUniqueId().toString(), null);
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        p.sendMessage(Data.getData().prefix + "§6Du erhälst nun wieder Anfragen!");

    }

    public boolean isAllowed(Player p) {

        file = new File(this.javaPlugin.getDataFolder().getPath(), "teleportConfig.yml");
        config = YamlConfiguration.loadConfiguration(file);

        if (config.getString(p.getUniqueId().toString()) != null) {
            if (config.getString(p.getUniqueId().toString()).equalsIgnoreCase("toggled")) {
                return false;
            }
        }

        return true;

    }

}
