package net.woodpixel.essentials.configuration;

import net.woodpixel.essentials.event.BlockDamageEvents;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class PlayerOptions {

    private File optionsFile;
    private YamlConfiguration configuration;
    private static PlayerOptions playerOptions;
    private JavaPlugin javaPlugin;
    private BlockDamageEvents blockEvents = BlockDamageEvents.getDamageEvents();

    public PlayerOptions(JavaPlugin javaPlugin) {
        playerOptions = this;
        this.javaPlugin = javaPlugin;
        optionsFile = new File(javaPlugin.getDataFolder(), "resumeData.yml");
        if (!optionsFile.exists()) {
            try {
                optionsFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        configuration = YamlConfiguration.loadConfiguration(optionsFile);
    }

    public static PlayerOptions getPlayerOptions() {
        return playerOptions;
    }

    public void savePlayerOptions(Player p) {

        GameMode gameMode = p.getGameMode();
        boolean godMode = blockEvents.isGod(p);

        configuration.set(p.getUniqueId().toString() + ".godMode", godMode);
        configuration.set(p.getUniqueId().toString() + ".gameMode", gameMode.toString());
        configuration.set(p.getUniqueId().toString() + ".flyState", p.getAllowFlight());
        configuration.set(p.getUniqueId().toString() + ".walkSpeed", Float.valueOf(p.getWalkSpeed() * 10.0F));
        configuration.set(p.getUniqueId().toString() + ".flySpeed", Float.valueOf(p.getFlySpeed() * 10.0F));
        try {
            configuration.save(optionsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void resumePlayerOptions(Player p) {

        if (configuration.getString(p.getUniqueId().toString()) != null) {

            if (configuration.getBoolean(p.getUniqueId().toString() + ".godMode")) {

                blockEvents.setGod(p);

            }

            if (configuration.getString(p.getUniqueId().toString() + ".gameMode") != null) {

                GameMode gameMode = Enum.valueOf(GameMode.class, configuration.getString(p.getUniqueId().toString() + ".gameMode"));
                p.setGameMode(gameMode);

            }

            if (configuration.get(p.getUniqueId().toString() + ".flyState") != null) {

                p.setAllowFlight(configuration.getBoolean(p.getUniqueId().toString() + ".flyState"));

            }

            if (configuration.get(p.getUniqueId().toString() + ".walkSpeed") != null) {

                int speed = configuration.getInt(p.getUniqueId().toString() + ".walkSpeed");

                float realSpeed = speed / 10.0F;

                p.setWalkSpeed(realSpeed);

            }

            if (configuration.get(p.getUniqueId().toString() + ".flySpeed") != null) {

                int speed = configuration.getInt(p.getUniqueId().toString() + ".flySpeed");

                float realSpeed = speed / 10.0F;

                p.setFlySpeed(realSpeed);

            }

        }

    }

}
