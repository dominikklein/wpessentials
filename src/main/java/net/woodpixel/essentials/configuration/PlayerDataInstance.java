package net.woodpixel.essentials.configuration;

import net.woodpixel.essentials.Main;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class PlayerDataInstance {

    private static File file;
    private static YamlConfiguration config;
    private static PlayerDataInstance pDI;

    private static Main main;

    public PlayerDataInstance() {
        pDI = this;
    }

    public static PlayerDataInstance getpDI() {
        return pDI;
    }

    public static void setupDataInstance() {

        /**file = new File(main.getInstance().getDataFolder().getPath() + "//playerData//", "playerConfig.yml");

         if (!file.exists()) {
         try {
         file.createNewFile();
         } catch (IOException e) {
         e.printStackTrace();
         }
         }

         config = YamlConfiguration.loadConfiguration(file);**/

    }

    public void createPlayerFile(OfflinePlayer p) {

        File file = new File(main.getInstance().getDataFolder().getPath() + "//playerData//", p.getUniqueId().toString() + ".yml");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("[DEBUG] User file coudn't be created!");
            }
        }

    }

    public boolean hasPlayerFile(OfflinePlayer p) {

        File file = new File(main.getInstance().getDataFolder().getPath() + "//playerData//", p.getUniqueId().toString() + ".yml");

        if (!file.exists()) {
           return false;
        }

        return true;

    }

    public File getPlayerFile(OfflinePlayer p) {

        if (hasPlayerFile(p)) {
            File file = new File(main.getInstance().getDataFolder().getPath() + "//playerData//", p.getUniqueId().toString() + ".yml");
            return file;
        }

        createPlayerFile(p);
        return new File(main.getInstance().getDataFolder().getPath() + "//playerData//", p.getUniqueId().toString() + ".yml");

    }

    public YamlConfiguration getPlayerConfig(OfflinePlayer p) {

        if (hasPlayerFile(p)) {
            File file = new File(main.getInstance().getDataFolder().getPath() + "//playerData//", p.getUniqueId().toString() + ".yml");
            return YamlConfiguration.loadConfiguration(file);
        }

        createPlayerFile(p);
        File file = new File(main.getInstance().getDataFolder().getPath() + "//playerData//", p.getUniqueId().toString() + ".yml");
        return YamlConfiguration.loadConfiguration(file);

    }

}
