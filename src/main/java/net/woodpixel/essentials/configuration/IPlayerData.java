package net.woodpixel.essentials.configuration;

import org.bukkit.entity.Player;

import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public interface IPlayerData {

    /**
     * Check configParam for null
     * @param field
     * @return boolean
     */
    boolean nullCheck(String field, Player p);

    /**
     * Add the last name to the list
     * of known names
     * @param p
     */
    void addLastName(Player p);

    /**
     * Add the last ip to the list
     * of known ips
     * @param p
     */
    void addLastIp(Player p);

    /**
     * Get the last ip of a player
     * @param p
     * @return last ip
     */
    String getLastIp(Player p);

    /**
     * Get the last name of a player
     * @param p
     * @return last name
     */
    String getLastName(Player p);

    /**
     * Get all known names
     * @param p
     * @return all known Names
     */
    List<String> getLastNames(Player p);

    /**
     * Get all known ips
     * @param p
     * @return all known ips
     */
    List<String> getLastIps(Player p);

}
