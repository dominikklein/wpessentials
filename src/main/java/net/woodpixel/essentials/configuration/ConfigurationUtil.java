package net.woodpixel.essentials.configuration;

import net.woodpixel.essentials.Data;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ConfigurationUtil {

    private JavaPlugin javaPlugin;

    public ConfigurationUtil(JavaPlugin javaPlugin) {

        this.javaPlugin = javaPlugin;
        createConfig();
        setupData();

    }

    private void createConfig() {

        HashMap<String, Object> setDefaults = new HashMap<String, Object>();
        setDefaults.put("main.prefix", "&8[&bCityBuild&8] &r");
        setDefaults.put("main.noPerm", "%pr%&cDazu hast du keine Rechte!");

        setDefaults.put("mysql.user", "root");
        setDefaults.put("mysql.password", "password");
        setDefaults.put("mysql.host", "127.0.0.1");
        setDefaults.put("mysql.port", 3306);
        setDefaults.put("mysql.database", "wpessentials");
        setDefaults.put("mysql.databaseEco", "userdata");
        setDefaults.put("mysql.tablePrefix", "wpess_");
        setDefaults.put("mysql.bankTable", "mpdb_economy");


        for (String s : setDefaults.keySet()) {

            this.javaPlugin.getConfig().addDefault(s, setDefaults.get(s));

        }

        this.javaPlugin.getConfig().options().copyDefaults(true);
        this.javaPlugin.saveConfig();

    }

    private String replacerColor(String color) {
        return color.replaceAll("&", "§");
    }

    private String replacerAll(String colorAndPrefix) {
        return replacerColor(colorAndPrefix).replaceAll("%pr%",
                this.javaPlugin.getConfig().getString("main.prefix"));
    }

    private void setupData() {

        Data data = Data.getData();

        data.prefix = replacerColor(this.javaPlugin.getConfig().getString("main.prefix"));
        data.noPerm = replacerAll(this.javaPlugin.getConfig().getString("main.noPerm"));

    }

}
