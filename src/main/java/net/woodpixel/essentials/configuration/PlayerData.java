package net.woodpixel.essentials.configuration;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class PlayerData implements IPlayerData {

    /**
     * PlayerData
     */

    private static PlayerData pD;

    public PlayerData() {
        pD = this;
    }

    public static PlayerData getPD() {
        return pD;
    }

    /**
     * Interface Section!
     */

    @Override
    public boolean nullCheck(String field, Player p) {

        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);

        if (configuration.get(field) == null) {
            return true;
        }

        return false;

    }

    @Override
    public void addLastName(Player p) {
        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);

        if (configuration.getStringList("playerData.names") == null) {
            configuration.set("playerData.names", Arrays.asList(p.getName()));
            configuration.set("playerData.lastName", p.getName());
        }
        else {
            List<String> names = configuration.getStringList("playerData.names");
            if (!names.contains(p.getName())) {
                names.add(p.getName());
            }
            configuration.set("playerData.names", names);
            configuration.set("playerData.lastName", p.getName());
            try {
                configuration.save(PlayerDataInstance.getpDI().getPlayerFile(p));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void addLastIp(Player p) {
        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);

        if (configuration.getStringList("playerData.ips") == null) {
            configuration.set("playerData.ips", Arrays.asList(p.getAddress().getAddress().getHostAddress()));
            configuration.set("playerData.lastIp", p.getAddress().getAddress().getHostAddress());
        }
        else {
            List<String> names = configuration.getStringList("playerData.ips");
            if (!names.contains(p.getAddress().getAddress().getHostAddress())) {
                names.add(p.getAddress().getAddress().getHostAddress());
            }
            configuration.set("playerData.ips", names);
            configuration.set("playerData.lastIp", p.getAddress().getAddress().getHostAddress());
            try {
                configuration.save(PlayerDataInstance.getpDI().getPlayerFile(p));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String getLastIp(Player p) {
        if (nullCheck("playerData.lastIp", p)) {
            return "-";
        }
        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);
        return configuration.getString("playerData.lastIp");
    }

    @Override
    public String getLastName(Player p) {
        if (nullCheck("playerData.lastName", p)) {
            return "-";
        }
        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);
        return configuration.getString("playerData.lastName");
    }

    @Override
    public List<String> getLastNames(Player p) {
        if (nullCheck("playerData.names", p)) {
            return Arrays.asList("-");
        }
        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);
        return configuration.getStringList("playerData.names");
    }

    @Override
    public List<String> getLastIps(Player p) {
        if (nullCheck("playerData.ips", p)) {
            return Arrays.asList("-");
        }
        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);
        return configuration.getStringList("playerData.ips");
    }
}
