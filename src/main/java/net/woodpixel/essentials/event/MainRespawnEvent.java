package net.woodpixel.essentials.event;

import net.woodpixel.essentials.commandUtils.SpawnProvider;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class MainRespawnEvent implements Listener {

    private JavaPlugin javaPlugin;

    public MainRespawnEvent(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onRespawn(PlayerRespawnEvent e) {
        new BukkitRunnable() {

            @Override
            public void run() {

                SpawnProvider.getSpawnProvider().loadSpawnLocation();
                e.getPlayer().teleport(SpawnProvider.getSpawnProvider().getSpawnLocation());

            }

        }.runTaskLaterAsynchronously(javaPlugin, 1L);

    }

}
