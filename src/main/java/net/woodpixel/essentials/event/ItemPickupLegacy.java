package net.woodpixel.essentials.event;

import net.woodpixel.essentials.commandUtils.VanishHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ItemPickupLegacy implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onItemPickUp(PlayerPickupItemEvent event) {

        if (VanishHelper.getVanishHelper().isVanished(event.getPlayer())) {
            if (!event.getPlayer().isSneaking()) {
                event.setCancelled(true);
            }
        }

    }

}
