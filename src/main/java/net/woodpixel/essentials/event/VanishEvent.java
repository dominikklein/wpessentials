package net.woodpixel.essentials.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class VanishEvent extends Event {

    private static final HandlerList HANDLER_LIST = new HandlerList();
    private final VanishStates state;
    private final Player player;

    public VanishEvent(Player player, VanishStates state) {
        this.state = state;
        this.player = player;
    }

    public enum VanishStates {
        UN_VANISH, VANISH
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public Player getPlayer() {
        return player;
    }

    public VanishStates getState() {
        return state;
    }
}
