package net.woodpixel.essentials.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class JoinFunctionEvent extends Event {

    private static HandlerList handlerList = new HandlerList();

    private final Player player;
    private final boolean isCompleted;

    public JoinFunctionEvent(Player player, Boolean isCompleted) {
        this.player = player;
        this.isCompleted = isCompleted;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

    public Player getPlayer() {
        return this.player;
    }

    public boolean getCompleted() {
        return this.isCompleted;
    }

}
