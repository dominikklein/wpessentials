package net.woodpixel.essentials.event;

import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.VanishHelper;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class BlockDamageEvents implements Listener {

    private JavaPlugin javaPlugin;

    private static BlockDamageEvents damageEvents;
    private List<Player> god = new ArrayList<>();

    public void setDamageEvents() {
        damageEvents = this;
    }

    public static BlockDamageEvents getDamageEvents() {
        return damageEvents;
    }

    public BlockDamageEvents(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    public void setGod(Player p) {
        god.add(p);
    }

    public void unSetGod(Player p) {
        god.remove(p);
    }

    public boolean isGod(Player p) {
        if (god.contains(p)) {
            return true;
        }
        else {
            return false;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDamage(EntityDamageEvent e) {

        if (e.getEntity() instanceof Player && god.contains(e.getEntity())) {
            e.setCancelled(true);
        }

        if (e.getEntity() instanceof Player && VanishHelper.getVanishHelper().isVanished(((Player) e.getEntity()).getKiller()) &&
            !PermissionUtil.getUtil().hasPermission(((Player) e.getEntity()).getKiller(), PermissionUtil.getUtil().PERM_VANISH_PVP)) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onFood(FoodLevelChangeEvent e) {

        if (god.contains(e.getEntity())) {
            e.setCancelled(true);
        }

    }

}
