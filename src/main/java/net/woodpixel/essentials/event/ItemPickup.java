package net.woodpixel.essentials.event;

import net.woodpixel.essentials.commandUtils.VanishHelper;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ItemPickup implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onItemPickup(EntityPickupItemEvent event) {

        if (event.getEntity() instanceof Player) {

            if (VanishHelper.getVanishHelper().isVanished((Player) event.getEntity())) {
                if (!((Player) event.getEntity()).isSneaking()) {
                    event.setCancelled(true);
                }
            }

        }

    }

}
