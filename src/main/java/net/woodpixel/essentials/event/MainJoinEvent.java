package net.woodpixel.essentials.event;

import net.woodpixel.essentials.BaseProvider;
import net.woodpixel.essentials.Main;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.SpawnProvider;
import net.woodpixel.essentials.configuration.PlayerOptions;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class MainJoinEvent implements Listener {

    private JavaPlugin javaPlugin;
    private static MainJoinEvent joinEvent;
    private PermissionUtil permUtil = PermissionUtil.getUtil();
    private PlayerOptions playerOptions = PlayerOptions.getPlayerOptions();

    public MainJoinEvent(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    public void setJoinEvent() {
        joinEvent = this;
    }

    public static MainJoinEvent getJoinEvent() {
        return joinEvent;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {

        this.javaPlugin.getServer().getPluginManager().callEvent(new NewPlayerEvent(e.getPlayer()));

        SyncListener.getSyncListener().syncIncomplete.add(e.getPlayer());
        BaseProvider.getBaseProvider().addPlayerElseUpdate(e.getPlayer());
        BaseProvider.getBaseProvider().downloadPlayer(e.getPlayer());
        BaseProvider.getBaseProvider().setCompleted(e.getPlayer());
        this.javaPlugin.getServer().getPluginManager().callEvent(new JoinFunctionEvent(e.getPlayer(), true));

        if (SpawnProvider.getSpawnProvider().getSpawnLocation() != null) {
            SpawnProvider.getSpawnProvider().loadSpawnLocation();
            e.getPlayer().teleport(SpawnProvider.getSpawnProvider().getSpawnLocation());
        }

        if (permUtil.hasPermission(e.getPlayer(), permUtil.PERM_KEEP_OPTIONS)) {

            playerOptions.resumePlayerOptions(e.getPlayer());

        }

        else {

            e.getPlayer().setGameMode(GameMode.SURVIVAL);
            e.getPlayer().setWalkSpeed(0.2F);
            e.getPlayer().setFlySpeed(0.2F);

        }

    }

}
