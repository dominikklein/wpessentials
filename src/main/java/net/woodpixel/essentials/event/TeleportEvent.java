package net.woodpixel.essentials.event;

import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class TeleportEvent implements Listener {

    @EventHandler
    public void onTeleport(PlayerTeleportEvent event) {

        DeathEvent.backLocations.put(event.getPlayer(), event.getFrom());

    }

    @EventHandler
    public void onWorldChange(PlayerChangedWorldEvent event) {
        if (!PermissionUtil.getUtil().hasPermission(event.getPlayer(), PermissionUtil.getUtil().PERM_KEEP_ON_WORLD_CHANGE)) {
            event.getPlayer().setAllowFlight(false);
            event.getPlayer().setGameMode(GameMode.SURVIVAL);
        }

    }

}
