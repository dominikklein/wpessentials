package net.woodpixel.essentials.event;

import net.woodpixel.essentials.BaseProvider;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.TeleportUtil;
import net.woodpixel.essentials.configuration.PlayerOptions;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class MainQuitEvent implements Listener {

    private JavaPlugin javaPlugin;
    private static MainQuitEvent quitEvent;
    private PermissionUtil permUtil = PermissionUtil.getUtil();
    private PlayerOptions playerOptions = PlayerOptions.getPlayerOptions();

    public MainQuitEvent(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    public void setJoinEvent() {
        quitEvent = this;
    }

    public static MainQuitEvent getQuitEvent() {
        return quitEvent;
    }

    @EventHandler
    public void onJoin(PlayerQuitEvent e) {

        BaseProvider.getBaseProvider().addPlayerElseUpdate(e.getPlayer());
        BaseProvider.getBaseProvider().uploadPlayer(e.getPlayer());

        if (permUtil.hasPermission(e.getPlayer(), permUtil.PERM_KEEP_OPTIONS)) {

            playerOptions.savePlayerOptions(e.getPlayer());

        }

        TeleportUtil.getTeleportUtil().removeOnQuit(e.getPlayer());

    }

}
