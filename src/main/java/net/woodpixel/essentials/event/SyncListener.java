package net.woodpixel.essentials.event;

import net.woodpixel.essentials.BaseProvider;
import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.eco.Economy;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class SyncListener implements Listener {

    private static SyncListener syncListener;
    public List<Player> syncIncomplete = new ArrayList<>();

    public SyncListener(JavaPlugin javaPlugin) {
        syncListener = this;
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    public static SyncListener getSyncListener() {
        return syncListener;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCommand(PlayerCommandPreprocessEvent e) {

        if (syncIncomplete.contains(e.getPlayer())) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(Data.getData().prefix + "§cDeine Daten wurden noch nicht vollständig verarbeitet!");
            return;
        }

    }

   @EventHandler
   public void onJoinFunction(JoinFunctionEvent event) {
        if (event.getCompleted()) {
            SyncListener.getSyncListener().syncIncomplete.remove(event.getPlayer());
        }
   }

   @EventHandler
    public void onNewPlayer(NewPlayerEvent event) {
        event.setNewPlayer(BaseProvider.getBaseProvider().existsBank(event.getPlayer()));
        if (!event.isNewPlayer()) {
            BaseProvider.getBaseProvider().addPlayerElseUpdate(event.getPlayer());
            Economy.getEco().addMoney(event.getPlayer(), 4000);
            BaseProvider.getBaseProvider().uploadPlayer(event.getPlayer());
            event.getPlayer().sendMessage(Data.getData().prefix + "§aDa du das erste mal gejoint bist wurden dir 4000$ gutgeschrieben!");
        }
   }

}
