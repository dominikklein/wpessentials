package net.woodpixel.essentials.event;

import com.intellectualcrafters.plot.api.PlotAPI;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class PlotListener implements Listener {

    private List<Player> chestBlock = new ArrayList<>();
    private PlotAPI api = new PlotAPI();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClickChest(PlayerInteractEvent e) {

        if (PermissionUtil.getUtil().hasPermission(e.getPlayer(), PermissionUtil.getUtil().PERM_CHEST_CONTROL)) {
            if (PermissionUtil.getUtil().hasPermission(e.getPlayer(), PermissionUtil.getUtil().PERM_CHEST_CONTROL_BYPASS)) {
                return;
            }

            if (!api.isInPlot(e.getPlayer())) {
                return;
            }

            File file = new File("plugins/WPSystem/Data/cases.yml");
            YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

            String world = config.getString("CaseChest" + ".Location" + ".world");

            double x = config.getDouble("CaseChest" + ".Location" + ".x");
            double y = config.getDouble("CaseChest" + ".Location" + ".y");
            double z = config.getDouble("CaseChest" + ".Location" + ".z");

            Location chestPlacedLocation = new Location(Bukkit.getWorld(world), x, y, z);

            BlockState state = e.getClickedBlock().getState();

            if (state instanceof Chest) {

                if (chestPlacedLocation.equals(e.getClickedBlock().getLocation())) return;

                if (!api.getPlot(e.getClickedBlock().getLocation()).isOwner(e.getPlayer().getUniqueId()) ||
                        !api.getPlot(e.getClickedBlock().getLocation()).isAdded(e.getPlayer().getUniqueId())) {

                    Inventory inventory = Bukkit.createInventory(null, 54, "§4§lCHESTCHECK");
                    Chest chest = (Chest)e.getClickedBlock().getState();
                    ItemStack[] itemStacks = chest.getInventory().getContents();
                    inventory.setContents(itemStacks);
                    chestBlock.add(e.getPlayer());
                    e.getPlayer().openInventory(inventory);
                }
            }

            if (state instanceof DoubleChest) {

                if (chestPlacedLocation.equals(e.getClickedBlock().getLocation())) return;

                if (!api.getPlot(e.getClickedBlock().getLocation()).isOwner(e.getPlayer().getUniqueId()) ||
                    !api.getPlot(e.getClickedBlock().getLocation()).isAdded(e.getPlayer().getUniqueId())) {

                    Inventory inventory = Bukkit.createInventory(null, 54, "§4§lCHESTCHECK");
                    DoubleChest chest = (DoubleChest) e.getClickedBlock().getState();
                    ItemStack[] itemStacks = chest.getInventory().getContents();
                    inventory.setContents(itemStacks);
                    chestBlock.add(e.getPlayer());
                    e.getPlayer().openInventory(inventory);
                }
            }
        }

    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (chestBlock.contains(e.getWhoClicked())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDrag(InventoryDragEvent e) {
        if (chestBlock.contains(e.getWhoClicked())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if (e.getInventory().getName().equals("§4§lCHESTCHECK")) {
            if (chestBlock.contains(e.getPlayer())) {
                chestBlock.remove(e.getPlayer());
            }
        }
    }

}
