package net.woodpixel.essentials.event;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.Main;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.eco.Economy;
import net.woodpixel.essentials.shop.Shop;
import net.woodpixel.essentials.shop.ShopClick;
import net.woodpixel.essentials.shop.ShopConfigurator;
import net.woodpixel.essentials.shop.ShopSoundHandler;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Creature;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.Map;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ShopInventoryEvents implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {

        if (event.getInventory().getName().equalsIgnoreCase("§6Shop")) {
            event.setCancelled(true);
            for (Map.Entry<ItemStack, ShopClick> entry : ShopConfigurator.mainInvClickedItem.entrySet()) {

                if (event.getCurrentItem().equals(entry.getKey())) {

                    switch (entry.getValue()) {

                        case EAT:
                            event.getWhoClicked().openInventory(ShopConfigurator.getShopConfigurator().iEat);
                            break;
                        case PERMS:
                            event.getWhoClicked().openInventory(ShopConfigurator.getShopConfigurator().iPerms);
                            break;
                        case RARE:
                            event.getWhoClicked().openInventory(ShopConfigurator.getShopConfigurator().iRare);
                            break;
                        case NORMAL:
                            event.getWhoClicked().openInventory(ShopConfigurator.getShopConfigurator().iNormal);
                            break;
                        case EXPENSIVE:
                            event.getWhoClicked().openInventory(ShopConfigurator.getShopConfigurator().iExpensive);
                            break;
                        case CLOSE:
                            event.getWhoClicked().closeInventory();
                            break;
                        case BACK:
                            event.getWhoClicked().openInventory(ShopConfigurator.iMain);
                            break;
                            
                    }

                }

            }

            for (Map.Entry<ItemStack, Double> entry : ShopConfigurator.getShopConfigurator().itemPriceMap.entrySet()) {

                ItemStack clicked = event.getCurrentItem();
                ItemStack map = entry.getKey();
                double price = entry.getValue();
                Player player = (Player) event.getWhoClicked();

                if (clicked.equals(map)) {

                    if (Shop.getShop().hasEnoughMoney(player, price)) {

                        if (event.getInventory().equals(ShopConfigurator.getShopConfigurator().iPerms)) {

                            Economy.getEco().removeMoney(player, price);

                            for (Map.Entry<ItemStack, String> entryPerms : ShopConfigurator.getShopConfigurator().permSectionName.entrySet()) {

                                ItemStack mapPerm = entryPerms.getKey();
                                String sectionName = entryPerms.getValue();

                                if (clicked.equals(mapPerm)) {

                                    File file = new File(Main.getInstance().getDataFolder(), "shopItems.yml");
                                    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

                                    String permission = config.getString("items.perms." + sectionName + ".permission");
                                    if (permission.toLowerCase().startsWith("plots.plot")) {
                                        String permsSub = permission.substring(11);
                                        int plotAmount = Integer.parseInt(permsSub);
                                        int toCheck = plotAmount - 1;
                                        if (!player.hasPermission("plots.plot." + toCheck)) {
                                            player.sendMessage(Data.getData().prefix + "§cDu musst zuerst das §b" + toCheck + ". Plot §ckaufen!");
                                            ShopSoundHandler.getHandler().playSoundFailed(player);
                                            return;
                                        }
                                    }
                                    if (player.hasPermission(permission)) {
                                        player.sendMessage(Data.getData().prefix + "§cDu besitzt dieses Recht bereits!");
                                        ShopSoundHandler.getHandler().playSoundFailed(player);
                                        return;
                                    }
                                    Shop.getShop().addPermission(player, permission);
                                    player.sendMessage(Data.getData().prefix + "§aDu hast " + clicked.getItemMeta().getDisplayName() + " §agekauft!");
                                    ShopSoundHandler.getHandler().playSoundSuccess(player);

                                }

                            }

                            return;

                        }

                        int amount = clicked.getAmount();
                        ItemStack is = new ItemStack(clicked.getType(), amount, clicked.getDurability());
                        if (Shop.getShop().isSpaceAvailable(player)) {
                            Economy.getEco().removeMoney(player, price);
                            player.getInventory().addItem(is);
                            player.sendMessage(Data.getData().prefix + "§aDu hast das Item erhalten!");
                            ShopSoundHandler.getHandler().playSoundSuccess(player);
                        }
                        else {
                            player.sendMessage(Data.getData().prefix + "§cDu hast nicht genug Platz im Inventar!");
                            ShopSoundHandler.getHandler().playSoundFailed(player);
                        }

                    }

                    else {
                        player.sendMessage(Data.getData().prefix + "§cDu hast nicht genug Geld!");
                        ShopSoundHandler.getHandler().playSoundFailed(player);
                    }

                }

            }

        }

    }

    @EventHandler
    public void onDamageGeneral(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player) && event.getEntity() instanceof Creature) {
            if (event.getEntityType() == EntityType.VILLAGER) {
                if (event.getEntity().isCustomNameVisible()) {
                    if (event.getEntity().getCustomName().equalsIgnoreCase("§b✢§cAdmin§eShop§b✢")) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player) && event.getEntity() instanceof Creature && event.getDamager() instanceof Player) {
            if (event.getEntityType() == EntityType.VILLAGER) {
                if (event.getEntity().isCustomNameVisible()) {
                    if (event.getEntity().getCustomName().equalsIgnoreCase("§b✢§cAdmin§eShop§b✢")) {
                        event.setCancelled(true);
                        Player player = (Player) event.getDamager();
                        player.openInventory(ShopConfigurator.iMain);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onInterAct(PlayerInteractEntityEvent event) {
        if (!(event.getRightClicked() instanceof Player) && event.getRightClicked() instanceof Creature) {
            if (event.getRightClicked().getType() == EntityType.VILLAGER) {
                if (event.getRightClicked().isCustomNameVisible()) {
                    if (event.getRightClicked().getCustomName().equalsIgnoreCase("§b✢§cAdmin§eShop§b✢")) {
                        event.setCancelled(true);
                        Player player = event.getPlayer();
                        player.closeInventory();
                        player.openInventory(ShopConfigurator.iMain);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onInterAct(PlayerInteractAtEntityEvent event) {
        if (!(event.getRightClicked() instanceof Player) && event.getRightClicked() instanceof Creature) {
            if (event.getRightClicked().getType() == EntityType.VILLAGER) {
                if (event.getRightClicked().isCustomNameVisible()) {
                    if (event.getRightClicked().getCustomName().equalsIgnoreCase("§b✢§cAdmin§eShop§b✢")) {
                        event.setCancelled(true);
                        Player player = event.getPlayer();
                        player.closeInventory();
                        player.openInventory(ShopConfigurator.iMain);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onVehicleEnter(VehicleEnterEvent event) {

        if (!(event.getEntered() instanceof Player) && event.getEntered() instanceof Creature) {
            if (event.getEntered().getType() == EntityType.VILLAGER) {
                if (event.getEntered().isCustomNameVisible()) {
                    if (event.getEntered().getCustomName().equalsIgnoreCase("§b✢§cAdmin§eShop§b✢")) {
                        event.setCancelled(true);
                    }
                }
            }
        }

    }

}
