package net.woodpixel.essentials.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class NewPlayerEvent extends Event {

    private static HandlerList handlerList = new HandlerList();

    private final Player player;
    private boolean isNewPlayer;

    public NewPlayerEvent(Player player) {
        this.player = player;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

    public Player getPlayer() {
        return this.player;
    }

    public boolean isNewPlayer() {
        return this.isNewPlayer;
    }

    public void setNewPlayer(Boolean isNewPlayer) {
        this.isNewPlayer = isNewPlayer;
    }

}
