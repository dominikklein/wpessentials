package net.woodpixel.essentials.event;

import net.woodpixel.essentials.commandUtils.FontHandler;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class SignEvent implements Listener {

    @EventHandler
    public void onSignChange(SignChangeEvent event) {

        for (int i = 0; i < 4; i++) {

            if (event.getLine(i) != null) {

                event.setLine(i, FontHandler.getFontHandler().calculate(event.getLine(i), event.getPlayer()));

            }

        }

    }

}
