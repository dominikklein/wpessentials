package net.woodpixel.essentials.event;

import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class SpamKickEvent implements Listener {

    @EventHandler
    public void onPlayerKick(PlayerKickEvent event) {
        if (PermissionUtil.getUtil().hasPermission(event.getPlayer(), PermissionUtil.getUtil().PERM_BYPASS_SPAM_DISCONNECT)) {
            if (event.getReason().equalsIgnoreCase("disconnect.spam")) {
                event.setCancelled(true);
                return;
            }
        }
    }

}
