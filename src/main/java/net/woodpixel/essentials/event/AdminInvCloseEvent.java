package net.woodpixel.essentials.event;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.commandUtils.AdminInvUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class AdminInvCloseEvent implements Listener {

    @EventHandler
    public void onClose(InventoryCloseEvent event) {

        if (event.getInventory().getName().startsWith("§5" + event.getPlayer().getName() + "-")) {
            int page = Integer.parseInt(event.getInventory().getName().split("-")[1]);
            AdminInvUtils.getUtils().saveInventory(event.getInventory(), page, (Player) event.getPlayer());
            event.getPlayer().sendMessage(Data.getData().prefix + "§aDas Inventar von §6" + event.getPlayer().getName() + " §awurde gespeichert!");
        }

        else if (event.getInventory().getName().startsWith("§5") && event.getInventory().getItem(45).getType() == Material.PAPER && event.getInventory().getItem(53).getType() == Material.PAPER) {
            OfflinePlayer player = Bukkit.getOfflinePlayer(event.getInventory().getName().split("-")[0].substring(2));
            int page = Integer.parseInt(event.getInventory().getName().split("-")[1]);
            AdminInvUtils.getUtils().saveInventory(event.getInventory(), page, player);
            event.getPlayer().sendMessage(Data.getData().prefix + "§aDas Inventar von §6" + player.getName() + " §awurde gespeichert!");
        }

    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {

        if (event.getInventory().getName().startsWith("§5")) {
            if (event.getCurrentItem().getType() == Material.PAPER) {
                if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cZurück")) {

                    event.setCancelled(true);
                    String name = event.getInventory().getName().split("-")[0].substring(2);
                    OfflinePlayer player = Bukkit.getOfflinePlayer(name);
                    int page = Integer.parseInt(event.getInventory().getName().split("-")[1]);
                    AdminInvUtils.getUtils().saveInventory(event.getClickedInventory(), page, player);
                    if (page <= 1) {
                        page = 2;
                    }

                    AdminInvUtils.getUtils().loadInventory(player, (Player)event.getWhoClicked(), page-1);

                }

                if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aWeiter")) {

                    event.setCancelled(true);
                    String name = event.getInventory().getName().split("-")[0].substring(2);
                    OfflinePlayer player = Bukkit.getOfflinePlayer(name);
                    int page = Integer.parseInt(event.getInventory().getName().split("-")[1]);
                    AdminInvUtils.getUtils().saveInventory(event.getClickedInventory(), page, player);
                    AdminInvUtils.getUtils().loadInventory(player, (Player)event.getWhoClicked(), page+1);

                }

            }

        }

    }

}
