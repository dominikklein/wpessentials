package net.woodpixel.essentials.event;

import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.VanishHelper;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class TargetEvent implements Listener {

    @EventHandler
    public void onEntityTarget(EntityTargetLivingEntityEvent event) {
        if (event.getTarget() instanceof Player) {
            if (VanishHelper.getVanishHelper().isVanished((Player)event.getTarget())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onTarget(EntityTargetEvent event) {
        if (event.getTarget() instanceof Player) {
            if (VanishHelper.getVanishHelper().isVanished((Player)event.getTarget())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPvp(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            if (VanishHelper.getVanishHelper().isVanished((Player) event.getDamager())) {
                if (!PermissionUtil.getUtil().hasPermission((Player) event.getDamager(), PermissionUtil.getUtil().PERM_VANISH_PVP) &&
                    event.getEntity() instanceof Player) {
                    event.setCancelled(true);
                }
            }
        }
    }

}
