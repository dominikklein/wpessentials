package net.woodpixel.essentials;

import net.woodpixel.essentials.configuration.ConfigurationUtil;
import net.woodpixel.essentials.configuration.PluginState;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class Main extends JavaPlugin {

    private static Main INSTANCE;
    public static String version;

    private PluginState pluginState;

    @Override
    public void onEnable() {

        INSTANCE = this;
        new Data();
        String a = this.getServer().getClass().getPackage().getName();
        String b = a.substring(a.lastIndexOf('.') + 1);
        version = b;
        new ConfigurationUtil(this); sendInitializer(ConfigurationUtil.class);
        Registerer registerer = new Registerer(this); sendInitializer(Registerer.class);
        pluginState = registerer.getPluginState();
        sendType(pluginState);


    }

    @Override
    public void onDisable() {

        INSTANCE = null;

    }



    /**
     * GETTER MAIN INSTANCE
     */

    @Deprecated
    public static Main getInstance() {
        return INSTANCE;
    }

    /**
     * Related Stuff
     */

    private void sendInitializer(Class<?> clazz) {

        Bukkit.getConsoleSender().sendMessage("§7[§cWPESS§7] §bInitialized necessary class: " + clazz);

    }

    private void sendType(PluginState pluginState) {

        switch (pluginState) {

            case COMPLETE:
                Bukkit.getConsoleSender().sendMessage("§7[§cWPESS§7] §aInitialized successfully");
                break;
            case ERROR:
                Bukkit.getConsoleSender().sendMessage("§7[§cWPESS§7] §cInitialized with errors. Plugin may not working correctly!");
                break;
            case EXCEPTION_THROWN:
                Bukkit.getConsoleSender().sendMessage("§7[§cWPESS§7] §cInitialized with exceptions. Plugin may not working correctly!");
                break;
            case DISABLED:
                Bukkit.getConsoleSender().sendMessage("§7[§cWPESS§7] §cDue an error the plugin will be disabled now for safety reasons!");
                Bukkit.getServer().getPluginManager().disablePlugin(this);
                break;

        }

    }

}
