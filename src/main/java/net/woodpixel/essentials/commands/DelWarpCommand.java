package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.Warp;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class DelWarpCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();
    Warp warp = Warp.getWarp();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_DELWARP)) {

                if (strings.length == 0) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/delwarp <WarpName>");
                    return true;

                }

                else if (strings.length == 1) {

                    if (warp.exists(strings[0])) {

                        warp.deleteWarp(strings[0]);
                        p.sendMessage(Data.getData().prefix + "§aDer Warp §6" + strings[0] + " §awurde gelöscht!");

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Warp existiert nicht!");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/delwarp <WarpName>");
                    return true;

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_DELWARP);

            }

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length == 0) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/delwarp <WarpName>");

            }

            else {

                if (warp.exists(strings[0])) {

                    warp.deleteWarp(strings[0]);
                    p.sendMessage(Data.getData().prefix + "§aDer Warp §6" + strings[0] + " §awurde gelöscht!");
                    Data.getData().consoleIssuedNotify("Issued command /delwarp " + strings[0]);

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Warp existiert nicht!");

                }

            }

        }

        return false;
    }
}
