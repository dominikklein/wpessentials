package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class GamemodeCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_GAMEMODE_BASE)) {

                if (strings.length == 1) {

                    int input = Integer.parseInt(strings[0]);

                    GameMode gmv;
                    String perm;

                    switch (input) {

                        case 0:
                            gmv = GameMode.SURVIVAL;
                            perm = permUtil.PERM_GAMEMODE_SURVIVAL;
                            setGameMode(p, perm, gmv, "§cÜberleben");
                            break;

                        case 1:
                            gmv = GameMode.CREATIVE;
                            perm = permUtil.PERM_GAMEMODE_CREATIVE;
                            setGameMode(p, perm, gmv, "§cKreativ");
                            break;

                        case 2:
                            gmv = GameMode.ADVENTURE;
                            perm = permUtil.PERM_GAMEMODE_ADVENTURE;
                            setGameMode(p, perm, gmv, "§cErlbenis");
                            break;

                        case 3:
                            gmv = GameMode.SPECTATOR;
                            perm = permUtil.PERM_GAMEMODE_SPECTATOR;
                            setGameMode(p, perm, gmv, "§cZuschauer");
                            break;

                    }

                }

                else if (strings.length == 2) {

                    if (!permUtil.hasPermission(p, permUtil.PERM_GAMEMODE_OTHER)) {
                        permUtil.returnNoPermission(p, permUtil.PERM_GAMEMODE_OTHER);
                        return true;
                    }

                    int input = Integer.parseInt(strings[0]);
                    Player target = Bukkit.getPlayer(strings[1]);

                    GameMode gmv;
                    String perm;

                    if (target != null) {

                        switch (input) {

                            case 0:
                                gmv = GameMode.SURVIVAL;
                                perm = permUtil.PERM_GAMEMODE_SURVIVAL;
                                setGameModeOther(p, target, perm, gmv, "§cÜberleben");
                                break;

                            case 1:
                                gmv = GameMode.CREATIVE;
                                perm = permUtil.PERM_GAMEMODE_CREATIVE;
                                setGameModeOther(p, target, perm, gmv, "§cKreativ");
                                break;

                            case 2:
                                gmv = GameMode.ADVENTURE;
                                perm = permUtil.PERM_GAMEMODE_ADVENTURE;
                                setGameModeOther(p, target, perm, gmv, "§cErlbenis");
                                break;

                            case 3:
                                gmv = GameMode.SPECTATOR;
                                perm = permUtil.PERM_GAMEMODE_SPECTATOR;
                                setGameModeOther(p, target, perm, gmv, "§cZuschauer");
                                break;

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/gamemode <0/1/2/3> [Spieler]");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_GAMEMODE_BASE);

            }

            return true;

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length != 2) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/gamemode <0/1/2/3> <Spieler>");

            }

            else {

                int input = Integer.parseInt(strings[0]);
                Player target = Bukkit.getPlayer(strings[1]);

                GameMode gmv;
                String perm;

                if (target != null) {

                    switch (input) {

                        case 0:
                            gmv = GameMode.SURVIVAL;
                            perm = permUtil.PERM_GAMEMODE_SURVIVAL;
                            setGameModeOtherConsole(p, target, perm, gmv, "§cÜberleben");
                            break;

                        case 1:
                            gmv = GameMode.CREATIVE;
                            perm = permUtil.PERM_GAMEMODE_CREATIVE;
                            setGameModeOtherConsole(p, target, perm, gmv, "§cKreativ");
                            break;

                        case 2:
                            gmv = GameMode.ADVENTURE;
                            perm = permUtil.PERM_GAMEMODE_ADVENTURE;
                            setGameModeOtherConsole(p, target, perm, gmv, "§cErlbenis");
                            break;

                        case 3:
                            gmv = GameMode.SPECTATOR;
                            perm = permUtil.PERM_GAMEMODE_SPECTATOR;
                            setGameModeOtherConsole(p, target, perm, gmv, "§cZuschauer");
                            break;

                    }

                    Data.getData().consoleIssuedNotify("Issued command /gamemode " + String.join(" ", strings));

                }

            }

        }

        return false;

    }

    private void setGameModeOther(Player p, Player target, String perm, GameMode gmv, String modeName) {

        if (permUtil.hasPermission(p, perm)) {

            target.setGameMode(gmv);
            target.sendMessage(Data.getData().prefix + "§6Dein Gamemode wurde auf " + modeName + " §6gesetzt!");
            p.sendMessage(Data.getData().prefix + "§6Der Gamemode von " + target.getDisplayName() +" §6wurde auf " + modeName + " §6gesetzt!");

        }

        else {

            permUtil.returnNoPermission(p, perm);

        }

    }

    private void setGameModeOtherConsole(ConsoleCommandSender p, Player target, String perm, GameMode gmv, String modeName) {

        target.setGameMode(gmv);
        target.sendMessage(Data.getData().prefix + "§6Dein Gamemode wurde auf " + modeName + " §6gesetzt!");
        p.sendMessage(Data.getData().prefix + "§6Der Gamemode von " + target.getDisplayName() +" §6wurde auf " + modeName + " §6gesetzt!");

    }

    private void setGameMode(Player p, String perm, GameMode gmv, String modeName) {

        if (permUtil.hasPermission(p, perm)) {

            p.setGameMode(gmv);
            p.sendMessage(Data.getData().prefix + "§6Dein Gamemode wurde auf " + modeName + " §6gesetzt!");

        }

        else {

            permUtil.returnNoPermission(p, perm);

        }

    }

}
