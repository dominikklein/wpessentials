package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.TeleportUtil;
import net.woodpixel.essentials.commandUtils.VanishHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class TpoCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_TPO)) {

                if (strings.length == 1) {

                    Player target = Bukkit.getPlayer(strings[0]);

                    if (target != null) {

                        if (VanishHelper.getVanishHelper().isVanished(target) && !permUtil.hasPermission(p, permUtil.PERM_VANISH_INTERACT)) {
                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");
                            return true;
                        }

                        p.teleport(target.getLocation());
                        p.sendMessage(Data.getData().prefix + "§aDu wurdest teleportiert!");

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                    }

                }

                else if (strings.length == 2) {

                    if (!permUtil.hasPermission(p, permUtil.PERM_TPOHERE)) {
                        permUtil.returnNoPermission(p, permUtil.PERM_TPOHERE);
                        return true;
                    }

                    Player target = Bukkit.getPlayer(strings[0]);
                    Player target2 = Bukkit.getPlayer(strings[1]);

                    if (target != null && target2 != null) {

                        if (VanishHelper.getVanishHelper().isVanished(target) && !permUtil.hasPermission(p, permUtil.PERM_VANISH_INTERACT)) {
                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");
                            return true;
                        }

                        if (permUtil.hasPermission(target, permUtil.PERM_TPOHERE_PREVENT) && !permUtil.hasPermission(p, permUtil.PERM_TPOHERE_BYPASS_PREVENT)) {
                            permUtil.returnNoPermission(p, permUtil.PERM_TPOHERE_BYPASS_PREVENT);
                            return true;
                        }

                        target.teleport(target2.getLocation());
                        p.sendMessage(Data.getData().prefix + "§b" + target.getName() + " §awurde zu §b" + target2.getName() + " §ateleportiert!");

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                    }

                }

                else if (strings.length == 3) {

                    if (!permUtil.hasPermission(p, permUtil.PERM_TPPOS)) {
                        permUtil.returnNoPermission(p, permUtil.PERM_TPPOS);
                        return true;
                    }

                    Location loc;

                    try {
                        int x = Integer.parseInt(strings[0]);
                        int y = Integer.parseInt(strings[1]);
                        int z = Integer.parseInt(strings[2]);
                        loc = new Location(p.getWorld(), x, y, z);
                    } catch (NumberFormatException e) {
                        p.sendMessage(Data.getData().prefix + "§cDiese Koordinaten sind ungültig!");
                        return true;
                    }

                    if (!TeleportUtil.getTeleportUtil().isWorldPermitted(p.getWorld().getName(), p, permUtil.PERM_TPPOS_PER_WORLD)) {
                        permUtil.returnNoPermission(p, permUtil.PERM_TPPOS_PER_WORLD + p.getWorld().getName());
                        return true;
                    }

                    p.teleport(loc);
                    p.sendMessage(Data.getData().prefix + "§aDu wurdest teleportiert!");

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/tpo <Spieler>");

                }

            }

            else {

                permUtil.returnNoPermission(p, PermissionUtil.getUtil().PERM_TPO);

            }

            return true;

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;

    }

}
