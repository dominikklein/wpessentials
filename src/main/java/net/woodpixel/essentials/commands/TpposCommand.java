package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.TeleportUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.ParseException;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class TpposCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_TPPOS)) {

                if (strings.length == 3) {

                    Location loc;

                    try {
                        int x = Integer.parseInt(strings[0]);
                        int y = Integer.parseInt(strings[1]);
                        int z = Integer.parseInt(strings[2]);
                        loc = new Location(p.getWorld(), x, y, z);
                    } catch (NumberFormatException e) {
                        p.sendMessage(Data.getData().prefix + "§cDiese Koordinaten sind ungültig!");
                        return true;
                    }

                    if (!TeleportUtil.getTeleportUtil().isWorldPermitted(p.getWorld().getName(), p, permUtil.PERM_TPPOS_PER_WORLD)) {
                        permUtil.returnNoPermission(p, permUtil.PERM_TPPOS_PER_WORLD + p.getWorld().getName());
                        return true;
                    }

                    p.teleport(loc);
                    p.sendMessage(Data.getData().prefix + "§aDu wurdest teleportiert!");

                }

                else if (strings.length == 4) {

                    Location loc;
                    int x;
                    int y;
                    int z;
                    World w;

                    try {
                        x = Integer.parseInt(strings[0]);
                        y = Integer.parseInt(strings[1]);
                        z = Integer.parseInt(strings[2]);
                    } catch (NumberFormatException e) {
                        p.sendMessage(Data.getData().prefix + "§cDiese Koordinaten sind ungültig!");
                        return true;
                    }

                    try {
                        w = Bukkit.getWorld(strings[3]);

                        if (!TeleportUtil.getTeleportUtil().isWorldPermitted(w.getName(), p, permUtil.PERM_TPPOS_PER_WORLD)) {
                            permUtil.returnNoPermission(p, permUtil.PERM_TPPOS_PER_WORLD + w.getName());
                            return true;
                        }

                        p.teleport(new Location(w, x, y, z));
                        p.sendMessage(Data.getData().prefix + "§aDu wurdest teleportiert!");
                    } catch (NullPointerException | CommandException | IllegalArgumentException e) {
                        p.sendMessage(Data.getData().prefix + "§cDiese Welt existiert nicht!");
                        return true;
                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung §6/tppos <x> <y> <z> [Welt]");

                }

            }

            else {

                permUtil.returnNoPermission(p, PermissionUtil.getUtil().PERM_TPPOS);

            }

            return true;

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;

    }

}
