package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ClearCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        PermissionUtil permUtil = PermissionUtil.getUtil();

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_CLEAR)) {

                if (strings.length == 0) {

                    p.getInventory().clear();
                    p.sendMessage(Data.getData().prefix + "§aDein Inventar wurde geleert!");

                }

                else if (strings.length == 1) {

                    if (permUtil.hasPermission(p, permUtil.PERM_CLEAR_OTHER)) {

                        Player target = Bukkit.getPlayer(strings[0]);

                        if (target != null) {

                            target.getInventory().clear();
                            p.sendMessage(Data.getData().prefix + "§aDas Inventar von " + target.getDisplayName() +
                                    " §awurde geleert!");

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                        }

                    }

                    else {

                        permUtil.returnNoPermission(p, permUtil.PERM_CLEAR_OTHER);

                    }

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_CLEAR);

            }

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length == 0) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/clear <Spieler>!");

            }

            else {

                Player target = Bukkit.getPlayer(strings[0]);

                if (target != null) {

                    target.getInventory().clear();
                    p.sendMessage(Data.getData().prefix + "§aDas Inventar von " + target.getDisplayName() +
                            " §awurde geleert!");

                    Data.getData().consoleIssuedNotify("Issued command /clear " + target.getName());

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                }

            }

        }

        return false;
    }
}
