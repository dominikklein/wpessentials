package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.SpawnProvider;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class SpawnCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_SPAWN)) {

                if (strings.length == 0) {
                    SpawnProvider.getSpawnProvider().loadSpawnLocation();
                    if (SpawnProvider.getSpawnProvider().getSpawnLocation() == null) {
                        if (permUtil.hasPermission(p, permUtil.PERM_SET_SPAWN)) {
                            p.sendMessage(Data.getData().prefix + "§cDer Spawnpunkt wurde noch nicht gesetzt!");
                        }
                        else {
                            p.sendMessage(Data.getData().prefix + "§cEs ist ein Fehler aufgetreten! Errorcode: 0");
                        }
                        return true;
                    }
                    p.teleport(SpawnProvider.getSpawnProvider().getSpawnLocation());
                    p.sendMessage(Data.getData().prefix + "§aDu bist nun am Spawn.");
                }

                else {
                    SpawnProvider.getSpawnProvider().loadSpawnLocation();
                    if (SpawnProvider.getSpawnProvider().getSpawnLocation() == null) {
                        if (permUtil.hasPermission(p, permUtil.PERM_SET_SPAWN)) {
                            p.sendMessage(Data.getData().prefix + "§cDer Spawnpunkt wurde noch nicht gesetzt!");
                        }
                        else {
                            p.sendMessage(Data.getData().prefix + "§cEs ist ein Fehler aufgetreten! Errorcode: 0");
                        }
                        return true;
                    }
                    p.teleport(SpawnProvider.getSpawnProvider().getSpawnLocation());
                    p.sendMessage(Data.getData().prefix + "§aDu bist nun am Spawn.");
                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_SPAWN);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
