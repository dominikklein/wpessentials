package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class HatCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_HAT)) {

                if (p.getItemInHand() != null && p.getItemInHand().getType() != Material.AIR) {

                    if (p.getEquipment().getHelmet() != null && p.getEquipment().getHelmet().getType() != Material.AIR) {

                        ItemStack itemStack = p.getEquipment().getHelmet();
                        ItemStack inHand = p.getItemInHand();
                        if (inHand.getAmount() > 1) {
                            inHand.setAmount(inHand.getAmount() - 1);
                        }
                        else {
                            p.setItemInHand(new ItemStack(Material.AIR));
                        }

                        p.getInventory().addItem(itemStack);
                        ItemStack is = new ItemStack(inHand.getType(), 1);
                        p.getEquipment().setHelmet(is);

                        p.sendMessage(Data.getData().prefix + "§aDu hast das Item nun auf dem Kopf!");

                    }

                    else {

                        ItemStack inHand = p.getItemInHand();
                        if (inHand.getAmount() > 1) {
                            inHand.setAmount(inHand.getAmount() - 1);
                        }
                        else {
                            p.setItemInHand(new ItemStack(Material.AIR));
                        }
                        ItemStack is = new ItemStack(inHand.getType(), 1);
                        p.getEquipment().setHelmet(is);

                        p.sendMessage(Data.getData().prefix + "§aDu hast das Item nun auf dem Kopf!");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDu musst ein Item in der Hand halten!");
                    return true;

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_HAT);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
