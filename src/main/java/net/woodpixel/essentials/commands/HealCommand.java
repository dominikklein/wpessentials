package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class HealCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_HEAL)) {

                if (strings.length == 0) {

                    p.setHealth(20);
                    p.setFoodLevel(20);
                    p.sendMessage(Data.getData().prefix + "§aDu wurdest geheilt!");

                }

                else if (strings.length == 1) {

                    if (permUtil.hasPermission(p, permUtil.PERM_HEAL_OTHER)) {

                        Player target = Bukkit.getPlayer(strings[0]);

                        if (target != null) {

                            target.setHealth(20);
                            target.setFoodLevel(20);
                            target.sendMessage(Data.getData().prefix + "§aDu wurdest geheilt!");
                            p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §awurde geheilt!");

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                        }

                    }

                    else {

                        permUtil.returnNoPermission(p, permUtil.PERM_HEAL_OTHER);

                    }

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_HEAL);

            }

            return true;

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length == 0) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/heal <Spieler>");

            }

            else if (strings.length == 1) {

                Player target = Bukkit.getPlayer(strings[0]);

                if (target != null) {

                    target.setHealth(20);
                    target.setFoodLevel(20);
                    target.sendMessage(Data.getData().prefix + "§aDu wurdest geheilt!");
                    p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §awurde geheilt!");
                    Data.getData().consoleIssuedNotify("Issued command /heal " + target.getName());

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                }

            }

        }

        return false;

    }
}
