package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class FlyCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_FLY)) {

                if (strings.length == 0) {

                    if (p.getAllowFlight()) {

                        p.setAllowFlight(false);
                        p.sendMessage(Data.getData().prefix + "§cDu kannst nun nicht mehr fliegen!");

                    }

                    else {

                        p.setAllowFlight(true);
                        p.sendMessage(Data.getData().prefix + "§aDu kannst nun fliegen!");

                    }

                }

                else if (strings.length == 1) {

                    Player target = Bukkit.getPlayer(strings[0]);

                    if (target != null) {

                        if (permUtil.hasPermission(p, permUtil.PERM_FLY_OTHER)) {

                            if (target.getAllowFlight()) {

                                target.sendMessage(Data.getData().prefix + "§cDu kannst nun nicht mehr fliegen!");
                                p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §ckann nun nicht mehr fliegen!");
                                target.setAllowFlight(false);

                            }

                            else {

                                target.sendMessage(Data.getData().prefix + "§aDu kannst nun fliegen!");
                                p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §akann nun fliegen!");
                                target.setAllowFlight(true);

                            }

                        }

                        else {

                            permUtil.returnNoPermission(p, permUtil.PERM_FLY_OTHER);

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/fly [Spieler]");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_FLY);

            }

            return true;

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length == 0) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/fly <Spieler>");

            }

            else {

                Player target = Bukkit.getPlayer(strings[0]);

                if (target != null) {

                    if (target.getAllowFlight()) {

                        target.sendMessage(Data.getData().prefix + "§cDu kannst nun nicht mehr fliegen!");
                        p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §ckann nun nicht mehr fliegen!");
                        target.setAllowFlight(false);

                    }

                    else {

                        target.sendMessage(Data.getData().prefix + "§aDu kannst nun fliegen!");
                        p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §akann nun fliegen!");
                        target.setAllowFlight(true);
                    }

                    Data.getData().consoleIssuedNotify("Issued command /fly " + target.getName());

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                }

            }

        }
        return false;
    }
}
