package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class PTimeCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_PTIME)) {

                if (strings.length != 1) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/ptime <day/night>");
                    return true;

                }

                else if (strings[0].matches("[0-9]+")) {

                    Long l = Long.parseLong(strings[0]);

                    if (l > 24000) {

                        p.sendMessage(Data.getData().prefix + "§cDie angegebene Zeit ist ungültig!");

                    }

                    else {

                        p.setPlayerTime(l, false);
                        p.sendMessage(Data.getData().prefix + "§6Deine Zeit wurde auf §c" + l + " §6Ticks gesetzt!");

                    }

                }

                else if (strings[0].equalsIgnoreCase("day")) {

                    p.setPlayerTime(3000L, false);
                    p.sendMessage(Data.getData().prefix + "§6Es ist nun §cTag §6für dich!");

                }

                else if (strings[0].equalsIgnoreCase("night")) {

                    p.setPlayerTime(14000L, false);
                    p.sendMessage(Data.getData().prefix + "§6Es ist nun §cNacht §6für dich!");

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/ptime <day/night>");
                    return true;

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_PTIME);

            }

            return true;

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;

    }
}
