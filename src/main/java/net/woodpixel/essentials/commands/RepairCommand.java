package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class RepairCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_REPAIR)) {

                if (strings.length == 0) {

                    ItemStack is = p.getItemInHand();
                    if (is == null || is.getType() == Material.AIR) {
                        p.sendMessage(Data.getData().prefix + "§cDu musst ein Item in der Hand halten!");
                        return true;
                    }
                    is.setDurability((short)0);

                    p.setItemInHand(is);
                    p.sendMessage(Data.getData().prefix + "§aDas Item wurde repariert!");

                }

                else {

                    ItemStack is = p.getItemInHand();
                    if (is == null) {
                        p.sendMessage(Data.getData().prefix + "§cDu musst ein Item in der Hand halten!");
                        return true;
                    }
                    is.setDurability((short)0);

                    p.setItemInHand(is);
                    p.sendMessage(Data.getData().prefix + "§aDas Item wurde repariert!");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_REPAIR);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
