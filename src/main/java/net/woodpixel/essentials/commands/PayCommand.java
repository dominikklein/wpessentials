package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.VanishHelper;
import net.woodpixel.essentials.eco.Economy;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class PayCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_PAY)) {

                if (strings.length == 0 || strings.length == 1) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/pay <Spieler> <Betrag>");

                }

                else if (strings.length == 2) {

                    String playerIp = p.getAddress().getAddress().getHostAddress();
                    Player target = Bukkit.getServer().getPlayer(strings[0]);
                    double balance = Economy.getEco().getMoney(p);
                    int payAmount = 0;

                    try {
                        payAmount = Integer.parseInt(strings[1]);
                    } catch (NumberFormatException e) {
                        p.sendMessage(Data.getData().prefix + "§cDer Betrag §6" + strings[1] + " §cist ungültig!");
                        return true;
                    }

                    if (target != null) {

                        if (VanishHelper.getVanishHelper().isVanished(target) && !permUtil.hasPermission(p, permUtil.PERM_VANISH_INTERACT)) {
                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler wurde nicht gefunden!");
                            return true;
                        }

                        String targetIp = target.getAddress().getAddress().getHostAddress();

                        if (target.equals(p)) {
                            p.sendMessage(Data.getData().prefix + "§cDu kannst dir selber nichts bezahlen!");
                            return true;
                        }

                        if (!permUtil.hasPermission(p, permUtil.PERM_PAY_SAMEIP) && playerIp.equals(targetIp)) {
                            p.sendMessage(Data.getData().prefix + "§cDu darfst kein Geld an Spieler mit der gleichen IP-Adresse zahlen!");
                            return true;
                        }

                        if (payAmount <= 0) {
                            p.sendMessage(Data.getData().prefix + "§cDer Betrag darf nicht weniger als §61$ §csein!");
                            return true;
                        }

                        if (!permUtil.hasPermission(p, permUtil.PERM_PAY_CREDIT) && balance - payAmount < 0) {
                            p.sendMessage(Data.getData().prefix + "§cDu kannst nicht mehr ausgeben als du auf dem Konto hast!");
                            return true;
                        }

                        Economy.getEco().removeMoney(p, payAmount);
                        Economy.getEco().addMoney(target, payAmount);
                        p.sendMessage(Data.getData().prefix + "§aDu hast " + target.getDisplayName() + " §aerfolgreich §6" + format(payAmount) + "$ §agezahlt!");
                        target.sendMessage(Data.getData().prefix + "§aDu hast von " + p.getDisplayName() + " §6" + format(payAmount) + "$ §aerhalten!");

                    }

                    else if (strings[0].equals("*")) {

                        if (permUtil.hasPermission(p, permUtil.PERM_PAY_ALL)) {

                            int amount = Integer.parseInt(strings[1]);

                            if (amount * Bukkit.getOnlinePlayers().size() > Economy.getEco().getMoney(p) && !permUtil.hasPermission(p, permUtil.PERM_PAY_CREDIT)) {
                                p.sendMessage(Data.getData().prefix + "§cDu hast nicht genug Geld!");
                                return true;
                            }

                            if (amount <= 0) {
                                p.sendMessage(Data.getData().prefix + "§cDer Betrag darf nicht weniger als §61$ §csein!");
                                return true;
                            }

                            for (Player all : Bukkit.getOnlinePlayers()) {

                                Economy.getEco().addMoney(all, amount);
                                Economy.getEco().removeMoney(p, amount);
                                p.sendMessage(Data.getData().prefix + "§aDu hast " + all.getDisplayName() + " §aerfolgreich §6" + format(amount) + "$ §agezahlt!");
                                all.sendMessage(Data.getData().prefix + "§aDu hast von " + p.getDisplayName() + " §6" + format(payAmount) + "$ §aerhalten!");

                            }

                        }

                        else {

                            permUtil.returnNoPermission(p, permUtil.PERM_PAY_ALL);

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler wurde nicht gefunden!");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/pay <Spieler> <Betrag>");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_PAY);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;

    }

    private String format(int amount) {

        NumberFormat nf = NumberFormat.getInstance(new Locale("de", "DE"));
        return nf.format(amount);

    }

}
