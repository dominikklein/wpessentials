package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.ItemAdaptor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ItemCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_ITEM)) {

                if (strings.length == 0) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/item <id[:subId]>");

                }

                else if (strings.length == 1) {

                    if (!strings[0].matches("[0-9]+")) {

                        String[] str = strings[0].split(":");
                        String subId;
                        try {
                            subId = str[1];
                        } catch (ArrayIndexOutOfBoundsException e) {
                            subId = "0";
                        }

                        int id = 1000000;

                        try {
                            id = Integer.parseInt(str[0]);
                        } catch (NumberFormatException e) {}

                        if (id != 1000000) {

                            ItemStack is = new ItemStack(Material.getMaterial(id), 1, Short.parseShort(subId));

                            p.getInventory().addItem(is);

                            p.sendMessage(Data.getData().prefix + "§aDu hast das Item mit der ID §6" + id + ":" + subId + " §aerhalten!");
                            return true;

                        }

                        short shortId = Short.parseShort(subId);

                        try {
                            ItemStack is = new ItemStack(Enum.valueOf(Material.class, str[0].toUpperCase()), 1, shortId);
                            p.getInventory().addItem(is);
                            p.sendMessage(Data.getData().prefix + "§aDu hast §6" + str[0] + " §aerhalten!");
                        } catch (NullPointerException | IllegalArgumentException e) {
                            if (ItemAdaptor.getAdaptor().getRequest(str[0]) != null) {
                                ItemStack is = new ItemStack(ItemAdaptor.getAdaptor().getRequest(str[0]), 1, shortId);
                                p.getInventory().addItem(is);
                                p.sendMessage(Data.getData().prefix + "§aDu hast §6" + str[0] + " §aerhalten!");
                            }
                            else {
                                p.sendMessage(Data.getData().prefix + "§6" + str[0] + " §cist kein gültiges Material");
                            }
                        }

                    }

                    else {

                        int id = Integer.parseInt(strings[0]);

                        ItemStack is = new ItemStack(Material.getMaterial(id), 1);

                        p.getInventory().addItem(is);

                        p.sendMessage(Data.getData().prefix + "§aDu hast das Item mit der ID §6" + id + " §aerhalten!");

                    }

                }

                else if (strings.length == 2) {

                    if (!strings[0].matches("[0-9]+")) {

                        String[] str = strings[0].split(":");
                        String subId;
                        int amount;
                        try {
                            amount = Integer.parseInt(strings[1]);
                        } catch (NumberFormatException e) {
                            amount = 1;
                        }
                        try {
                            subId = str[1];
                        } catch (ArrayIndexOutOfBoundsException e) {
                            subId = "0";
                        }

                        int id = 1000000;

                        try {
                            id = Integer.parseInt(str[0]);
                        } catch (NumberFormatException e) {}

                        if (id != 1000000) {

                            ItemStack is = new ItemStack(Material.getMaterial(id), amount, Short.parseShort(subId));

                            p.getInventory().addItem(is);

                            p.sendMessage(Data.getData().prefix + "§aDu hast das Item mit der ID §6" + id + ":" + subId + " §aerhalten!");
                            return true;

                        }

                        short shortId = Short.parseShort(subId);

                        try {
                            ItemStack is = new ItemStack(Enum.valueOf(Material.class, str[0].toUpperCase()), amount, shortId);
                            p.getInventory().addItem(is);
                            p.sendMessage(Data.getData().prefix + "§aDu hast §6" + str[0] + " §aerhalten!");
                        } catch (NullPointerException | IllegalArgumentException e) {
                            if (ItemAdaptor.getAdaptor().getRequest(str[0]) != null) {
                                ItemStack is = new ItemStack(ItemAdaptor.getAdaptor().getRequest(str[0]), 1, shortId);
                                p.getInventory().addItem(is);
                                p.sendMessage(Data.getData().prefix + "§aDu hast §6" + str[0] + " §aerhalten!");
                            }
                            else {
                                p.sendMessage(Data.getData().prefix + "§6" + str[0] + " §cist kein gültiges Material");
                            }
                        }

                    }

                    else {

                        int amount;
                        try {
                            amount = Integer.parseInt(strings[1]);
                        } catch (NumberFormatException e) {
                            amount = 1;
                        }

                        int id = Integer.parseInt(strings[0]);

                        ItemStack is = new ItemStack(Material.getMaterial(id), amount);

                        p.getInventory().addItem(is);

                        p.sendMessage(Data.getData().prefix + "§aDu hast das Item mit der ID §6" + id + " §aerhalten!");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/item <id[:subId]>");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_ITEM);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
