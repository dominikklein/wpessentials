package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.WeatherType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ToggleweatherCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_TOGGLEWEATHER)) {

                if (strings.length == 0) {

                    if (WeatherType.CLEAR == p.getPlayerWeather()) {

                        p.setPlayerWeather(WeatherType.DOWNFALL);
                        p.sendMessage(Data.getData().prefix + "§6Es regnet nun!");

                    }

                    else {

                        p.setPlayerWeather(WeatherType.CLEAR);
                        p.sendMessage(Data.getData().prefix + "§6Es scheint nun die Sonne!");

                    }

                }

                else {

                    if (WeatherType.CLEAR == p.getPlayerWeather()) {

                        p.setPlayerWeather(WeatherType.DOWNFALL);
                        p.sendMessage(Data.getData().prefix + "§6Es regnet nun!");

                    }

                    else {

                        p.setPlayerWeather(WeatherType.CLEAR);
                        p.sendMessage(Data.getData().prefix + "§6Es scheint nun die Sonne!");

                    }

                }

            }

            else {

                permUtil.returnNoPermission(p, PermissionUtil.getUtil().PERM_TOGGLEWEATHER);

            }

            return true;

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;

    }

}
