package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.Warp;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class WarpCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();
    Warp warp = Warp.getWarp();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_WARP)) {

                if (strings.length == 0) {

                    try {

                        List<String> warps = warp.getWarps();

                        String warp = "";

                        for (int i = 0; i < warps.size(); i++) {

                            warp += warps.get(i) + ", ";

                        }

                        warp = warp.substring(0, warp.length() - 2);
                        p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/warp <WarpName>");
                        p.sendMessage(Data.getData().prefix + "§aWarps: §6" + warp);

                    } catch (Exception e) {
                        p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/warp <WarpName>");
                        p.sendMessage(Data.getData().prefix + "§aWarps: §6-");
                    }


                    return true;

                }

                else if (strings.length == 1) {

                    if (warp.exists(strings[0])) {

                        if (warp.existsWorld(strings[0])) {

                            p.teleport(warp.getWarpLocation(strings[0]));
                            p.sendMessage(Data.getData().prefix + "§aDu wurdest zum Warp §6" + strings[0].toLowerCase() + " §ateleportiert!");

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cDie Welt des Warps existiert nicht mehr!");

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Warp existiert nicht!");

                    }

                }

                else {

                    try {

                        List<String> warps = warp.getWarps();

                        String warp = "";

                        for (int i = 0; i < warps.size(); i++) {

                            warp += warps.get(i) + ", ";

                        }

                        warp = warp.substring(0, warp.length() - 2);
                        p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/warp <WarpName>");
                        p.sendMessage(Data.getData().prefix + "§aWarps: §6" + warp);
                        return true;

                    } catch (Exception e) {
                        p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/warp <WarpName>");
                        p.sendMessage(Data.getData().prefix + "§aWarps: §6-");
                    }

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_WARP);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
