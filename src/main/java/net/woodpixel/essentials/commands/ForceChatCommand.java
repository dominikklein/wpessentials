package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

import java.util.Arrays;

public class ForceChatCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_FORCECHAT)) {

                if (strings.length <= 1) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/forcechat <Spieler> <Nachricht>");
                    return true;

                }

                Player target = Bukkit.getPlayer(strings[0]);

                if (target != null) {

                    if (permUtil.hasPermission(target, permUtil.PERM_FORCECHAT_PREVENT)) {

                        p.sendMessage(Data.getData().prefix + "§cDu kannst diesen Spieler nichts schreiben lassen!");
                        return true;

                    }

                    String[] message = Arrays.copyOfRange(strings, 1, strings.length);
                    String string = String.join(" ", message);
                    target.chat(string.replaceAll("&", "§"));
                    return true;

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");
                    return true;

                }

            }

            else {

                permUtil.returnNoPermission(p, PermissionUtil.getUtil().PERM_FORCECHAT);

            }

            return true;

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length <= 1) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/forcechat <Spieler> <Nachricht>");
                return true;

            }

            Player target = Bukkit.getPlayer(strings[0]);

            if (target != null) {

                String[] message = Arrays.copyOfRange(strings, 1, strings.length);
                String string = String.join(" ", message);
                target.chat(string.replaceAll("&", "§"));
                Data.getData().consoleIssuedNotify("Issued command /forcechat " + target.getName());
                return true;

            }

            else {

                p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");
                return true;

            }

        }

    }

}
