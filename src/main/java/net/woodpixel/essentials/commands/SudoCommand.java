package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class SudoCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_SUDO)) {

                if (strings.length < 2) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/sudo <Spieler> <Command>");


                }

                else {

                    Player target = Bukkit.getPlayer(strings[0]);

                    if (target != null) {

                        if (permUtil.hasPermission(target, permUtil.PERM_SUDO_PREVENT)) {
                            p.sendMessage(Data.getData().prefix + "§cDu kannst diesen Spieler keine Commands ausführen lassen!");
                            return true;
                        }

                        String[] cmd = Arrays.copyOfRange(strings, 1, strings.length);
                        String newCommand = String.join(" ", cmd);

                        target.performCommand(newCommand);
                        p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §ahat den Command §6" + newCommand + " §aausgeführt!");

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                    }

                }

            }

            else {

                permUtil.returnNoPermission(p, PermissionUtil.getUtil().PERM_SUDO);

            }

            return true;

        }

        else {

            if (strings.length == 0) {
                commandSender.sendMessage(Data.getData().prefix + "§cVerwendung: §6/sudo <Spieler> <Command>");
                return true;
            }

            Player target = Bukkit.getPlayer(strings[0]);

            if (target != null) {

                String[] cmd = Arrays.copyOfRange(strings, 1, strings.length);
                String newCommand = String.join(" ", cmd);

                target.performCommand(newCommand);
                commandSender.sendMessage(Data.getData().prefix + target.getDisplayName() + " §ahat den Command §6" + newCommand + " §aausgeführt!");
                Data.getData().consoleIssuedNotify("Issued command /sudo " + String.join(" ", strings));

            }

            else {

                commandSender.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

            }
            return false;

        }

    }

}
