package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class RenameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player player = (Player)commandSender;
            PermissionUtil perm = PermissionUtil.getUtil();

            if (perm.hasPermission(player, perm.PERM_RENAME)) {

                ItemStack is = player.getInventory().getItemInHand();
                if (is != null) {

                    if (strings.length <= 0) {
                        player.sendMessage(Data.getData().prefix + "§cVerwendung: §6/rename <Text>");
                    }

                    else {
                        ItemMeta im = is.getItemMeta();
                        im.setDisplayName(String.join(" ", strings));
                        is.setItemMeta(im);
                        player.setItemInHand(is);
                        player.sendMessage(Data.getData().prefix + "§aDas Item wurde umbenannt!");
                    }

                }

                else {
                    player.sendMessage(Data.getData().prefix + "§cDu musst ein Item in der Hand halten!");
                }

            }

            else {

                perm.returnNoPermission(player, perm.PERM_RENAME);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
