package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class RemoveLoreLineCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player player = (Player)commandSender;
            PermissionUtil perm = PermissionUtil.getUtil();

            if (perm.hasPermission(player, perm.PERM_REMOVE_LORE_LINE)) {

                if (strings.length < 1) {

                    player.sendMessage(Data.getData().prefix + "§cVerwendung: §6/removeloreline <Zeile>");

                }

                else {

                    ItemStack is = player.getInventory().getItemInHand();

                    if (is != null && is.getType() != Material.AIR) {

                        ItemMeta im = is.getItemMeta();
                        if (im.hasLore()) {

                            List<String> lore = im.getLore();

                            int providedLine = 0;

                            try {
                                providedLine = Integer.parseInt(strings[0]);
                            } catch (NumberFormatException e) {
                                player.sendMessage(Data.getData().prefix + "§cDie Zahl §6" + strings[0] + " §cist ungültig!");
                                return true;
                            }

                            if (providedLine <= 0 || lore.size() < providedLine) {
                                player.sendMessage(Data.getData().prefix + "§cEs sind nur die Zeilen von §61 §cbis §6" + lore.size() + " §cgültig!");
                            }

                            else {

                                providedLine = providedLine - 1;

                                lore.remove(lore.get(providedLine));
                                im.setLore(lore);
                                is.setItemMeta(im);
                                player.getInventory().setItemInHand(is);
                                player.sendMessage(Data.getData().prefix + "§aDie Zeile §6" + providedLine + " §awurde entfernt");

                            }

                        } else {
                            player.sendMessage(Data.getData().prefix + "§cDieses Item hat keine Lore!");
                        }

                    } else {

                        player.sendMessage(Data.getData().prefix + "§cDu musst ein Item in der Hand halten!");

                    }

                }

            }

            else {
                perm.returnNoPermission(player, perm.PERM_REMOVE_LORE_LINE);
            }

        }


        else {
            Data.getData().returnNoConsoleCommand(commandSender, command);
        }

        return false;
    }
}
