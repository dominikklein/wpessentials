package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.VanishHelper;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class NearCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_NEAR)) {

                if (strings.length == 0) {

                    if (permUtil.hasPermission(p, permUtil.PERM_NEAR_VANISHED)) {

                        List<String> nearby = new ArrayList<>();
                        List<String> nearbyHidden = new ArrayList<>();
                        double range = 75;

                        for (Entity e : p.getNearbyEntities(range, range, range)) {

                            if (e instanceof Player) {
                                if (VanishHelper.getVanishHelper().isVanished((Player)e)) {
                                    nearbyHidden.add(e.getName());
                                }
                                else {
                                    nearby.add(e.getName());
                                }
                            }

                        }

                        String nearHiden = "";
                        String near = "";

                        try {

                            if (!nearbyHidden.isEmpty()) {

                                for (int i = 0; i < nearbyHidden.size(); i++) {

                                    nearHiden += nearbyHidden.get(i) + ", ";

                                }

                                nearHiden = nearHiden.substring(0, nearHiden.length() - 2);

                            }

                            if (!nearby.isEmpty()) {

                                for (int i = 0; i < nearby.size(); i++) {

                                    near += nearby.get(i) + ", ";

                                }

                                near = near.substring(0, near.length() - 2);

                            }

                        } catch (Exception e) {}

                        p.sendMessage(Data.getData().prefix + "§cSpieler in der Nähe");
                        if (nearHiden.equalsIgnoreCase("")) {
                            p.sendMessage(Data.getData().prefix + "§eVersteckt: §6keine");
                        }
                        else {
                            p.sendMessage(Data.getData().prefix + "§eVersteckt: §6" + nearHiden);
                        }
                        if (near.equalsIgnoreCase("")) {
                            p.sendMessage(Data.getData().prefix + "§aNicht versteckt: §6keine");
                        }
                        else {
                            p.sendMessage(Data.getData().prefix + "§aNicht versteckt: §6" + near);
                        }

                    }

                    else {

                        List<String> nearby = new ArrayList<>();
                        double range = 75;

                        for (Entity e : p.getNearbyEntities(range, range, range)) {

                            if (e instanceof Player) {
                                if (!VanishHelper.getVanishHelper().isVanished((Player)e)) {
                                    nearby.add(e.getName());
                                }
                            }

                        }

                        String near = "";
                        try {

                            if (!nearby.isEmpty()) {

                                for (int i = 0; i < nearby.size(); i++) {

                                    near += nearby.get(i) + ", ";

                                }

                                near = near.substring(0, near.length() - 2);

                            }

                        } catch (Exception e) {}

                        if (near.equalsIgnoreCase("")) {
                            p.sendMessage(Data.getData().prefix + "§cSpieler in der Nähe: §6keine");
                        }
                        else {
                            p.sendMessage(Data.getData().prefix + "§cSpieler in der Nähe: §6" + near);
                        }

                    }

                }

                else {



                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_NEAR);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
