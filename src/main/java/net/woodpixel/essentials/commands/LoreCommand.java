package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class LoreCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player player = (Player)commandSender;
            PermissionUtil perm = PermissionUtil.getUtil();

            if (perm.hasPermission(player, perm.PERM_LORE)) {

                if (strings.length <= 0) {
                    player.sendMessage(Data.getData().prefix + "§cVerwendung: §6/lore <Text>");
                }

                else {

                    ItemStack is = player.getInventory().getItemInHand();

                    if (is != null) {
                        ItemMeta im = is.getItemMeta();
                        im.setLore(Arrays.asList(String.join(" ", strings).replaceAll("&", "§")));
                        is.setItemMeta(im);
                        player.setItemInHand(is);
                        player.sendMessage(Data.getData().prefix + "§aDie Lore wurde hinzugefügt!");
                    }

                    else {
                        player.sendMessage(Data.getData().prefix + "§cDu musst ein Item in der Hand halten!");
                    }

                }

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
