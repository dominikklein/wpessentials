package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.event.DeathEvent;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class BackCommand implements CommandExecutor {

    private PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_BACK)) {

                if (strings.length == 0) {

                    if (DeathEvent.backLocations.containsKey(p)) {

                        p.teleport(DeathEvent.backLocations.get(p));
                        p.sendMessage(Data.getData().prefix + "§aDu wurdest zurück teleportiert!");

                    } else {

                        p.sendMessage(Data.getData().prefix + "§cEs gibt keinen Punkt zum zurückkehren!");

                    }

                }

                else if (strings.length == 1) {

                    if (!permUtil.hasPermission(p, permUtil.PERM_BACK_OTHER)) {
                        permUtil.returnNoPermission(p, permUtil.PERM_BACK_OTHER);
                        return true;
                    }

                    OfflinePlayer target = Bukkit.getOfflinePlayer(strings[0]);

                    if (target != null) {

                        if (!DeathEvent.backLocations.containsKey(target)) {

                            p.sendMessage(Data.getData().prefix + "§cDer Spieler wurde nicht gefunden!");

                        }

                        else {

                            p.teleport(DeathEvent.backLocations.get(target));
                            p.sendMessage(Data.getData().prefix + "§aDu wurdest zurück teleportiert! §c(Spieler: §b" + target.getName() + "§c)");

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDer Spieler wurde nicht gefunden!");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/back [Spieler]");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_BACK);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
