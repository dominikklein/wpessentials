package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class EnchantCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_ENCHANT)) {

                HashMap<Integer, Enchantment> enchantments = new HashMap<>();
                for (Enchantment enchantment : Enchantment.values()) {

                    int id = enchantment.getId();
                    String enchantmentName = enchantment.getName();

                    enchantments.put(id, Enchantment.getByName(enchantmentName));

                }

                if (strings.length == 2) {

                    if (strings[0].matches("[0-9]+")) {

                        int id = Integer.parseInt(strings[0]);

                        if (enchantments.get(id) == null) {
                            p.sendMessage(Data.getData().prefix + "§cDie Enchantment-ID §6" + id + " §cexistiert nicht!");
                            return true;
                        }

                        int level = 1;

                        try {
                            level = Integer.parseInt(strings[1]);
                        } catch (NumberFormatException e) {
                            p.sendMessage(Data.getData().prefix + "§cDas Level §6" + strings[1] + " §cist ungültig!");
                            return true;
                        }

                        if (level > 32767) {

                            p.sendMessage(Data.getData().prefix + "§cDas maximale Level liegt bei §632767§c.");
                            return true;

                        } else {

                            ItemStack is = p.getItemInHand();
                            if (is != null && is.getType() != Material.AIR) {

                                ItemMeta im = is.getItemMeta();
                                im.addEnchant(enchantments.get(id), level, true);
                                if (id == 111) {
                                    im.spigot().setUnbreakable(true);
                                }
                                is.setItemMeta(im);
                                p.setItemInHand(is);
                                p.sendMessage(Data.getData().prefix + "§aDas Item wurde verzaubert!");

                            } else {
                                p.sendMessage(Data.getData().prefix + "§cDu musst ein Item in der Hand halten!");
                                return true;
                            }

                        }

                    }
                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/enchant <enchantId> <level>");
                    p.sendMessage(Data.getData().prefix + "§cFolgende Enchantments sind verfügbar:");
                    for (Enchantment enchantment : Enchantment.values()) {

                        p.sendMessage(Data.getData().prefix + "§6ID: §c" + enchantment.getId() + " §6» Name: §c" + enchantment.getName());

                    }
                    return true;

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_ENCHANT);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
