package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.TeleportUtil;
import net.woodpixel.essentials.commandUtils.VanishHelper;
import net.woodpixel.essentials.configuration.TptoggleConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class TpaCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_TPA)) {

                if (strings.length != 1) {
                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/tpa <Spieler>");
                    return true;
                }

                Player target = Bukkit.getPlayer(strings[0]);

                if (target != null) {

                    if (VanishHelper.getVanishHelper().isVanished(target) && !permUtil.hasPermission(p, permUtil.PERM_VANISH_INTERACT)) {
                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist offline!");
                        return true;
                    }

                    if (target.equals(p)) {
                        p.sendMessage(Data.getData().prefix + "§cDu kannst dir selbst keine Anfrage schicken!");
                        return true;
                    }

                    if (!TptoggleConfiguration.getConfiguration().isAllowed(target)) {
                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler möchte keine Anfragen erhalten!");
                        return true;
                    }

                    TeleportUtil.getTeleportUtil().sendTpa(p, target);

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist offline!");

                }

            }

            else {

                permUtil.returnNoPermission(p, PermissionUtil.getUtil().PERM_TPA);

            }

            return true;

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }
        return false;

    }
}
