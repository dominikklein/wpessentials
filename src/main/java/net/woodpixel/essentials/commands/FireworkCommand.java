package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class FireworkCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player player = (Player)commandSender;
            PermissionUtil perm = PermissionUtil.getUtil();

            if (perm.hasPermission(player, perm.PERM_FIREWORK)) {

                int limit = 15;
                int amount = 1;

                if (player.getInventory().getItemInHand().getType() != Material.FIREWORK) {
                    player.sendMessage(Data.getData().prefix + "§cDu musst eine Rakete in der Hand halten!");
                    return true;
                }

                if (strings.length >= 1) {

                    try {
                        amount = Integer.parseInt(strings[0]);
                    } catch (NumberFormatException e) {
                        player.sendMessage(Data.getData().prefix + "§cDie Zahl §6'" + strings[0] + "' §cist ungültig!");
                    }

                }

                if (!perm.hasPermission(player, perm.PERM_FIREWORK_BYPASS_LIMIT) && amount > limit) {
                    perm.returnNoPermission(player, perm.PERM_FIREWORK_BYPASS_LIMIT);
                    player.sendMessage(Data.getData().prefix + "§cDie Anzahl der Raketen wurde auf 15 angepasst!");
                    amount = limit;
                }

                for (int i = 0; i < amount; i++) {
                    Firework firework = (Firework) player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
                    FireworkMeta fmeta = (FireworkMeta) player.getInventory().getItemInHand().getItemMeta();
                    firework.setFireworkMeta(fmeta);
                }

            }
            else {
                perm.returnNoPermission(player, perm.PERM_FIREWORK);
            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
