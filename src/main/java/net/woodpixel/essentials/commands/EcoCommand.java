package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.eco.Economy;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class EcoCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_ECO)) {

                if (strings.length != 3) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/eco <set/remove/add> <Spieler> <Betrag>");
                    return true;

                }

                else if (strings.length == 3) {

                    String caseString = strings[0].toLowerCase();

                    int money = 0;

                    try {
                        money = Integer.parseInt(strings[2]);
                    } catch (NumberFormatException e) {
                        p.sendMessage(Data.getData().prefix + "§cDer Betrag §6" + strings[2] + " §cist ungültig!");
                        return true;
                    }

                    OfflinePlayer target = Bukkit.getOfflinePlayer(strings[1]);

                    if (target.hasPlayedBefore() && target != null) {

                        switch (caseString) {

                            case "set":
                                Economy.getEco().setPlayersMoney(target, money);
                                p.sendMessage(Data.getData().prefix + "§aDu hast das Konto von §b" + target.getName() + " §aauf §6" + money + "$ §agesetzt!");
                                break;
                            case "remove":
                                Economy.getEco().removeMoney(target, money);
                                p.sendMessage(Data.getData().prefix + "§aDu hast §b" + target.getName() + " §6" + money + "$ §aentfernt!");
                                break;
                            case "add":
                                Economy.getEco().addMoney(target, money);
                                p.sendMessage(Data.getData().prefix + "§aDu hast §b" + target.getName() + " §6" + money + "$ §ahinzugefügt!");
                                break;

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler existiert nicht!");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/eco <set/remove/add> <Spieler>");
                    return true;

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_ECO);

            }

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length != 3) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/eco <set/remove/add> <Spieler> <Betrag>");
                return true;

            }

            else if (strings.length == 3) {

                String caseString = strings[0].toLowerCase();

                int money = 0;

                try {
                    money = Integer.parseInt(strings[2]);
                } catch (NumberFormatException e) {
                    p.sendMessage(Data.getData().prefix + "§cDer Betrag §6" + strings[2] + " §cist ungültig!");
                    return true;
                }

                OfflinePlayer target = Bukkit.getOfflinePlayer(strings[1]);

                if (target.hasPlayedBefore() && target != null) {

                    switch (caseString) {

                        case "set":
                            Economy.getEco().setPlayersMoney(target, money);
                            p.sendMessage(Data.getData().prefix + "§aDu hast das Konto von §b" + target.getName() + " §aauf §6" + money + "$ §agesetzt!");
                            break;
                        case "remove":
                            Economy.getEco().removeMoney(target, money);
                            p.sendMessage(Data.getData().prefix + "§aDu hast §b" + target.getName() + " §6" + money + "$ §aentfernt!");
                            break;
                        case "add":
                            Economy.getEco().addMoney(target, money);
                            p.sendMessage(Data.getData().prefix + "§aDu hast §b" + target.getName() + " §6" + money + "$ §ahinzugefügt!");
                            break;

                    }

                    Data.getData().consoleIssuedNotify("Issued command /eco " + String.join(" ", strings));

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Spieler existiert nicht!");

                }

            }

            else {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/eco <set/remove/add> <Spieler>");
                return true;

            }

        }

        return false;
    }

}
