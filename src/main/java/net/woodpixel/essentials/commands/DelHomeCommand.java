package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.HomeManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class DelHomeCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_DELHOME)) {

                if (strings.length == 0) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/delhome <HomeName>");

                }

                else if (strings.length == 1) {

                    String homeName = strings[0];

                    if (HomeManager.getHomeManager().exists(p, homeName)) {

                        HomeManager.getHomeManager().delHome(p, homeName);
                        p.sendMessage(Data.getData().prefix + "§aDu hast das Home §6" + homeName.toLowerCase() + " §agelöscht!");

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieses Home existiert nicht!");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/delhome <HomeName>");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_DELHOME);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
