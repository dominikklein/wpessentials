package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class SetLoreLineCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player player = (Player)commandSender;
            PermissionUtil perm = PermissionUtil.getUtil();

            if (perm.hasPermission(player, perm.PERM_SET_LORE_LINE)) {

                ItemStack is = player.getItemInHand();

                if (is != null && is.getType() != Material.AIR) {

                    if (strings.length <= 1) {
                        player.sendMessage(Data.getData().prefix + "§cVerwendung: §6/setloreline <Line> <Text>");
                    }

                    else {
                        int line;
                        try {
                            line = Integer.parseInt(strings[0]);
                        } catch (NumberFormatException e) {
                            player.sendMessage(Data.getData().prefix + "§cDie Zahl §6" + strings[0] + " §cist ungültig!");
                            return true;
                        }

                        if (line <= 0) {
                            player.sendMessage(Data.getData().prefix + "§cErlaubt sind nur Zahlen ab §61 §caufwärts!");
                            return true;
                        }

                        ItemMeta im = is.getItemMeta();
                        line = line -1;

                        String[] loreArray = Arrays.copyOfRange(strings, 1, strings.length);
                        String loreLine = String.join(" ", loreArray);
                        List<String> lore = im.getLore();
                        try {
                            if (lore == null) {
                                lore = new ArrayList<>();
                                for (int i = 0; i < line-1; i++) {
                                    lore.add("");
                                }
                                lore.add(loreLine);
                            }
                            else {
                                lore.set(line, loreLine);
                            }
                        } catch (IndexOutOfBoundsException | NullPointerException e) {
                            if (lore == null) {
                                lore = new ArrayList<>();
                                for (int i = 0; i < line-1; i++) {
                                    lore.add("");
                                }
                                lore.add(loreLine);
                            }
                            else {
                                while (line != lore.size() - 1) {
                                    lore.add("");
                                }
                                lore.add(loreLine);
                            }
                        }
                        im.setLore(lore);
                        is.setItemMeta(im);
                        player.getInventory().setItemInHand(is);
                        player.sendMessage(Data.getData().prefix + "§aDie Lore in Zeile §6" + (line + 1) + " §awurde geändert!");
                    }

                }

                else {

                    player.sendMessage(Data.getData().prefix + "§cDu musst ein Item in der Hand halten!");

                }

            }

            else {
                perm.returnNoPermission(player, perm.PERM_SET_LORE_LINE);
            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
