package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.SpawnProvider;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class SetSpawnCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_SET_SPAWN)) {

                if (strings.length == 0) {
                    SpawnProvider.getSpawnProvider().setSpawnLocation(p.getLocation());
                    p.sendMessage(Data.getData().prefix + "§aDer Spawn wurde gesetzt!");
                }

                else {
                    SpawnProvider.getSpawnProvider().setSpawnLocation(p.getLocation());
                    p.sendMessage(Data.getData().prefix + "§aDer Spawn wurde gesetzt!");
                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_SET_SPAWN);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
