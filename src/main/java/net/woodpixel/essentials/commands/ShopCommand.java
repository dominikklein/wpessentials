package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.shop.Shop;
import net.woodpixel.essentials.shop.ShopConfigurator;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ShopCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player player = (Player)commandSender;

            if (strings.length == 0) {
                if (PermissionUtil.getUtil().hasPermission(player, PermissionUtil.getUtil().PERM_SHOP_ADMIN)) {
                    player.sendMessage(Data.getData().prefix + "§c==================================");
                    player.sendMessage(Data.getData().prefix + "§e/itemshop reload §7- §cShop neuladen");
                    player.sendMessage(Data.getData().prefix + "§e/itemshop spawn §7- §cShopNPC spawnen");
                    player.sendMessage(Data.getData().prefix + "§e/itemshop kill §7- §cShopNPC entfernen");
                    player.sendMessage(Data.getData().prefix + "§c==================================");
                }

                player.openInventory(ShopConfigurator.iMain);

            }

            else if (strings.length == 1) {

                if (PermissionUtil.getUtil().hasPermission(player, PermissionUtil.getUtil().PERM_SHOP_ADMIN) &&
                        strings[0].equalsIgnoreCase("reload")) {
                    new ShopConfigurator();
                    player.sendMessage(Data.getData().prefix + "§a'shopItems.yml' wurde neu geladen!");
                    return true;
                }
                else if (PermissionUtil.getUtil().hasPermission(player, PermissionUtil.getUtil().PERM_SHOP_ADMIN) &&
                        strings[0].equalsIgnoreCase("spawn")) {
                    Shop.getShop().spawnShopNPC(player.getLocation());
                    player.sendMessage(Data.getData().prefix + "§aDer Villager wurde gespawnt!");
                    return true;
                }
                else if (PermissionUtil.getUtil().hasPermission(player, PermissionUtil.getUtil().PERM_SHOP_ADMIN) &&
                        strings[0].equalsIgnoreCase("kill")) {

                    LivingEntity villager = null;

                    for(Entity e : player.getNearbyEntities(3, 3, 3)) {
                        if(e.getType() == EntityType.VILLAGER){
                            villager = (LivingEntity) e;
                        }
                    }

                    if (villager == null) {
                        player.sendMessage(Data.getData().prefix + "§cDu musst den Villager anschauen!");
                        return true;
                    }

                    Location eye = player.getEyeLocation();
                    Vector toEntity = villager.getEyeLocation().toVector().subtract(eye.toVector());
                    double dot = toEntity.normalize().dot(eye.getDirection());

                    if (dot < 0.99D) {
                        if (villager.isCustomNameVisible()) {
                            if (villager.getCustomName().equalsIgnoreCase("§b✢§cAdmin§eShop§b✢")) {
                                villager.remove();
                                player.sendMessage(Data.getData().prefix + "§aDer Villager wurde entfernt!");
                            }
                        }
                    }

                    return true;
                }
                player.openInventory(ShopConfigurator.iMain);

            }

            else {

                player.openInventory(ShopConfigurator.iMain);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
