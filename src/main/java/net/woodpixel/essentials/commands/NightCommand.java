package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class NightCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_NIGHT)) {

                if (strings.length == 0) {

                    p.getWorld().setTime(14000L);
                    p.sendMessage(Data.getData().prefix + "§aEs ist nun Nacht.");

                }

                else if (strings.length == 1) {

                    try {
                        World world = Bukkit.getWorld(strings[0]);
                        world.setTime(14000L);
                        p.sendMessage(Data.getData().prefix + "§aEs ist nun Nacht in §6" + strings[0] + "§a.");
                    } catch (NullPointerException | CommandException | IllegalArgumentException e) {
                        p.sendMessage(Data.getData().prefix + "§cDie Welt §6" + strings[0] + " §cexistiert nicht!");
                        return true;
                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/night [Welt]");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_NIGHT);

            }

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length == 0) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/night <Welt>");

            }

            else {

                try {
                    World world = Bukkit.getWorld(strings[0]);
                    world.setTime(14000L);
                    p.sendMessage(Data.getData().prefix + "§aEs ist nun Nacht in §6" + strings[0] + "§a.");
                    Data.getData().consoleIssuedNotify("Issued command /night. Set time to 14000 in world " + world.getName());
                } catch (NullPointerException | CommandException | IllegalArgumentException e) {
                    p.sendMessage(Data.getData().prefix + "§cDie Welt §6" + strings[0] + " §cexistiert nicht!");
                    return true;
                }

            }

        }


        return false;
    }
}
