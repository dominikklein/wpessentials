package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class RainCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_RAIN)) {

                if (strings.length == 0) {

                    p.getWorld().setStorm(true);
                    p.sendMessage(Data.getData().prefix + "§aEs regnet nun in §6" + p.getWorld().getName());

                }

                else if (strings.length >= 1) {

                    String world = strings[0];

                    try {

                        World w = Bukkit.getWorld(world);
                        w.setStorm(true);
                        p.sendMessage(Data.getData().prefix + "§aEs regnet nun in §6" + w.getName());

                    } catch (NullPointerException | CommandException | IllegalArgumentException e) {
                        p.sendMessage(Data.getData().prefix + "§cDie Welt §6" + world + " §cexistiert nicht!");
                    }

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_RAIN);

            }

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length == 0) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/rain <Welt>");

            }

            else if (strings.length >= 1) {

                String world = strings[0];

                try {

                    World w = Bukkit.getWorld(world);
                    w.setStorm(true);
                    p.sendMessage(Data.getData().prefix + "§aEs regnet nun in §6" + w.getName());
                    Data.getData().consoleIssuedNotify("Issued command /rain. Set weather to rain in world " + w.getName());

                } catch (NullPointerException | CommandException | IllegalArgumentException e) {
                    p.sendMessage(Data.getData().prefix + "§cDie Welt §6" + world + " §cexistiert nicht!");
                }

            }

        }

        return false;
    }
}
