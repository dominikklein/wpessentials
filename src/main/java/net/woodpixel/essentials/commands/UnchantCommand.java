package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class UnchantCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;
            PermissionUtil perm = PermissionUtil.getUtil();

            if (perm.hasPermission(p, perm.PERM_UNCHANT)) {

                HashMap<Integer, Enchantment> enchantments = new HashMap<>();
                for (Enchantment enchantment : Enchantment.values()) {

                    int id = enchantment.getId();
                    String enchantmentName = enchantment.getName();

                    enchantments.put(id, Enchantment.getByName(enchantmentName));

                }

                if (strings.length == 1) {

                    int id;

                    try {
                         id = Integer.parseInt(strings[0]);
                    } catch (NumberFormatException e) {
                        p.sendMessage(Data.getData().prefix + "§cDie ID §6" + strings[0] + " §cist ungültig!");
                        return true;
                    }

                    if (p.getInventory().getItemInHand() == null || p.getInventory().getItemInHand() == null) {
                        p.sendMessage(Data.getData().prefix + "§cDu musst ein Item in der Hand halten!");
                        return true;
                    }

                    ItemStack is = p.getInventory().getItemInHand();
                    ItemMeta im = is.getItemMeta();
                    if (im.hasEnchant(Enchantment.getById(id))) {
                        im.removeEnchant(Enchantment.getById(id));
                        if (id == 111) {
                            im.spigot().setUnbreakable(false);
                        }
                    }
                    else {
                        p.sendMessage(Data.getData().prefix + "§cDas Enchantment ist nicht auf diesem Item!");
                        return true;
                    }
                    is.setItemMeta(im);
                    p.getInventory().setItemInHand(is);

                    p.sendMessage(Data.getData().prefix + "§aDas Enchantment mit der ID §6" + id + " §awurde entfernt!");

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/unchant <enchantId>");
                    p.sendMessage(Data.getData().prefix + "§cFolgende Enchantments sind verfügbar:");
                    for (Enchantment enchantment : Enchantment.values()) {
                        p.sendMessage(Data.getData().prefix + "§6ID: §c" + enchantment.getId() + " §6» Name: §c" + enchantment.getName());
                    }
                    return true;

                }

            }

            else {

                perm.returnNoPermission(p, perm.PERM_UNCHANT);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
