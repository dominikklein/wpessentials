package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.Main;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.VanishHelper;
import net.woodpixel.essentials.event.VanishEvent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class VanishCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_VANISH)) {

                if (strings.length == 0) {

                    if (VanishHelper.getVanishHelper().isVanished(p)) {

                        VanishHelper.getVanishHelper().unvanishPlayer(p);
                        p.sendMessage(Data.getData().prefix + "§cDu bist nun wieder sichtbar für alle!");

                    }

                    else {

                        if (permUtil.hasPermission(p, permUtil.PERM_VANISH_SEE_TEAM) && !permUtil.hasPermission(p, permUtil.PERM_VANISH_SEE_ALL)) {

                            VanishHelper.getVanishHelper().vanishPlayer(p, "team");
                            p.sendMessage(Data.getData().prefix + "§aDu bist nun für Spieler unsichtbar!");
                            Main.getInstance().getServer().getPluginManager().callEvent(new VanishEvent(p, VanishEvent.VanishStates.VANISH));

                        }

                        else if (permUtil.hasPermission(p, permUtil.PERM_VANISH_SEE_ALL)) {

                            VanishHelper.getVanishHelper().vanishPlayer(p, "admin");
                            p.sendMessage(Data.getData().prefix + "§aDu bist nun für Spieler unsichtbar!");
                            Main.getInstance().getServer().getPluginManager().callEvent(new VanishEvent(p, VanishEvent.VanishStates.UN_VANISH));

                        }

                    }

                }

                else if (strings.length == 1) {

                    if (permUtil.hasPermission(p, permUtil.PERM_VANISH_OTHER)) {

                        Player target = Bukkit.getPlayer(strings[0]);

                        if (target != null) {

                            if (VanishHelper.getVanishHelper().isVanished(target)) {

                                VanishHelper.getVanishHelper().unvanishPlayer(target);
                                target.sendMessage(Data.getData().prefix + "§cDu bist nun wieder sichtbar für alle!");
                                p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §cist nun wieder sichtbar!");
                                Main.getInstance().getServer().getPluginManager().callEvent(new VanishEvent(target, VanishEvent.VanishStates.UN_VANISH));
                                return true;

                            }

                            if (!permUtil.hasPermission(target, permUtil.PERM_VANISH)) {

                                VanishHelper.getVanishHelper().vanishPlayer(target, "team");
                                target.sendMessage(Data.getData().prefix + "§aDu bist nun für Spieler unsichtbar!");
                                p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §aist nun unsichtbar!");
                                Main.getInstance().getServer().getPluginManager().callEvent(new VanishEvent(target, VanishEvent.VanishStates.VANISH));

                            }

                            else if (permUtil.hasPermission(target, permUtil.PERM_VANISH_SEE_TEAM) && !permUtil.hasPermission(target, permUtil.PERM_VANISH_SEE_ALL)) {

                                VanishHelper.getVanishHelper().vanishPlayer(target, "team");
                                target.sendMessage(Data.getData().prefix + "§aDu bist nun für Spieler unsichtbar!");
                                p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §aist nun unsichtbar!");
                                Main.getInstance().getServer().getPluginManager().callEvent(new VanishEvent(target, VanishEvent.VanishStates.VANISH));

                            }

                            else if (permUtil.hasPermission(target, permUtil.PERM_VANISH_SEE_ALL)) {

                                VanishHelper.getVanishHelper().vanishPlayer(target, "admin");
                                target.sendMessage(Data.getData().prefix + "§aDu bist nun für Spieler unsichtbar!");
                                p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §aist nun unsichtbar!");
                                Main.getInstance().getServer().getPluginManager().callEvent(new VanishEvent(target, VanishEvent.VanishStates.VANISH));

                            }

                            else {

                                p.sendMessage(Data.getData().prefix + "§cError!");

                            }

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                        }

                    }

                    else {

                        permUtil.returnNoPermission(p, permUtil.PERM_VANISH_OTHER);

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/vanish [Spieler]");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_VANISH);

            }

            return true;

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            Player target = Bukkit.getPlayer(strings[0]);

            if (target != null) {

                if (VanishHelper.getVanishHelper().isVanished(target)) {

                    VanishHelper.getVanishHelper().unvanishPlayer(target);
                    target.sendMessage(Data.getData().prefix + "§cDu bist nun wieder sichtbar für alle!");
                    p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §cist nun wieder sichtbar!");
                    Main.getInstance().getServer().getPluginManager().callEvent(new VanishEvent(target, VanishEvent.VanishStates.UN_VANISH));
                    return true;

                }

                if (!permUtil.hasPermission(target, permUtil.PERM_VANISH)) {

                    VanishHelper.getVanishHelper().vanishPlayer(target, "team");
                    target.sendMessage(Data.getData().prefix + "§aDu bist nun für Spieler unsichtbar!");
                    p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §aist nun unsichtbar!");
                    Main.getInstance().getServer().getPluginManager().callEvent(new VanishEvent(target, VanishEvent.VanishStates.VANISH));

                }

                else if (permUtil.hasPermission(target, permUtil.PERM_VANISH_SEE_TEAM) && !permUtil.hasPermission(target, permUtil.PERM_VANISH_SEE_ALL)) {

                    VanishHelper.getVanishHelper().vanishPlayer(target, "team");
                    target.sendMessage(Data.getData().prefix + "§aDu bist nun für Spieler unsichtbar!");
                    p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §aist nun unsichtbar!");
                    Main.getInstance().getServer().getPluginManager().callEvent(new VanishEvent(target, VanishEvent.VanishStates.VANISH));

                }

                else if (permUtil.hasPermission(target, permUtil.PERM_VANISH_SEE_ALL)) {

                    VanishHelper.getVanishHelper().vanishPlayer(target, "admin");
                    target.sendMessage(Data.getData().prefix + "§aDu bist nun für Spieler unsichtbar!");
                    p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §aist nun unsichtbar!");
                    Main.getInstance().getServer().getPluginManager().callEvent(new VanishEvent(target, VanishEvent.VanishStates.VANISH));

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cError!");

                }

                Data.getData().consoleIssuedNotify("Issued command /vanish " + target.getName());

            }

            else {

                p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

            }

        }

        return false;

    }
}
