package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.HomeManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

import java.util.List;

public class HomeAdminCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_HOMEADMIN)) {

                if (strings.length == 0) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/homeadmin <list/del/set/tp> <Spieler> [Home]");

                }

                else if (strings.length == 2) {

                    String sub = strings[0];

                    OfflinePlayer target = Bukkit.getOfflinePlayer(strings[1]);

                    if (target.hasPlayedBefore()) {

                        if (sub.equalsIgnoreCase("list")) {

                            if (HomeManager.getHomeManager().listHomes(target).isEmpty()) {
                                p.sendMessage(Data.getData().prefix + "§aHomes von §e" + target.getName() + "§a: §6-");
                            }

                            else {

                                List<String> homes = HomeManager.getHomeManager().listHomes(target);

                                String home = "";

                                for (int i = 0; i < homes.size(); i++) {

                                    home += homes.get(i) + ", ";

                                }

                                home = home.substring(0, home.length() - 2);
                                p.sendMessage(Data.getData().prefix + "§aHomes von §e" + target.getName() + "§a: §6" + home);

                            }

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/homeadmin <list/del/set/tp> <Spieler> [Home]");

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler existiert nicht!");

                    }

                }

                else if (strings.length == 3) {

                    String sub = strings[0];

                    OfflinePlayer target = Bukkit.getOfflinePlayer(strings[1]);

                    String homeName = strings[2].toLowerCase();

                    if (target.hasPlayedBefore()) {

                        if (sub.equalsIgnoreCase("del")) {

                            if (HomeManager.getHomeManager().exists(target, homeName)) {

                                HomeManager.getHomeManager().delHome(target, homeName);
                                p.sendMessage(Data.getData().prefix + "§aDu hast das Home §6" + homeName.toLowerCase() + " §agelöscht!");

                            }

                            else {

                                p.sendMessage(Data.getData().prefix + "§cDieses Home existiert nicht!");

                            }

                        }

                        else if (sub.equalsIgnoreCase("set")) {

                            if (!HomeManager.getHomeManager().exists(target, homeName)) {

                                HomeManager.getHomeManager().setHome(target, p.getLocation(), homeName);
                                p.sendMessage(Data.getData().prefix + "§aDu hast den Homepunkt §6" + homeName.toLowerCase() + " §agesetzt!");

                            }

                            else {

                                p.sendMessage(Data.getData().prefix + "§cDieses Home existiert bereits!");

                            }

                        }

                        else if (sub.equalsIgnoreCase("tp")) {

                            List<String> homes = HomeManager.getHomeManager().listHomes(target);

                            if (homes.contains(homeName)) {

                                p.teleport(HomeManager.getHomeManager().getHome(target, homeName));
                                p.sendMessage(Data.getData().prefix + "§aDu wurdest zum Home §6" + homeName + " §ateleportiert!");

                            }

                            else {

                                p.sendMessage(Data.getData().prefix + "§cDas Home §6" + homeName + " §cexistiert nicht!");
                                return true;

                            }

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/homeadmin <list/del/set/tp> <Spieler> [Home]");

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler existiert nicht!");

                    }



                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/homeadmin <list/del/set/tp> <Spieler> [Home]");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_HOMEADMIN);

            }

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length == 0) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/homeadmin <list/del> <Spieler> [Home]");

            }

            else if (strings.length == 2) {

                String sub = strings[0];

                OfflinePlayer target = Bukkit.getOfflinePlayer(strings[1]);

                if (target.hasPlayedBefore()) {

                    if (sub.equalsIgnoreCase("list")) {

                        if (HomeManager.getHomeManager().listHomes(target).isEmpty()) {
                            p.sendMessage(Data.getData().prefix + "§aHomes von §e" + target.getName() + "§a: §6-");
                        }

                        else {

                            List<String> homes = HomeManager.getHomeManager().listHomes(target);

                            String home = "";

                            for (int i = 0; i < homes.size(); i++) {

                                home += homes.get(i) + ", ";

                            }

                            home = home.substring(0, home.length() - 2);
                            p.sendMessage(Data.getData().prefix + "§aHomes von §e" + target.getName() + "§a: §6" + home);

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/homeadmin <list/del> <Spieler> [Home]");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Spieler existiert nicht!");

                }

            }

            else if (strings.length == 3) {

                String sub = strings[0];

                OfflinePlayer target = Bukkit.getOfflinePlayer(strings[1]);

                String homeName = strings[2].toLowerCase();

                if (target.hasPlayedBefore()) {

                    if (sub.equalsIgnoreCase("del")) {

                        if (HomeManager.getHomeManager().exists(target, homeName)) {

                            HomeManager.getHomeManager().delHome(target, homeName);
                            p.sendMessage(Data.getData().prefix + "§aDu hast das Home §6" + homeName.toLowerCase() + " §agelöscht!");

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cDieses Home existiert nicht!");

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/homeadmin <list/del> <Spieler> [Home]");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Spieler existiert nicht!");

                }



            }

            else {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/homeadmin <list/del> <Spieler> [Home]");

            }

        }

        return false;
    }
}
