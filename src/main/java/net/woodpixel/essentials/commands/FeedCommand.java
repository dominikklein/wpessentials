package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class FeedCommand implements CommandExecutor {

    private PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_FEED)) {

                if (strings.length == 0) {

                    p.setFoodLevel(20);
                    p.sendMessage(Data.getData().prefix + "§aDein Hunger wurde gestillt!");

                }

                else if (strings.length == 1) {

                    if (!permUtil.hasPermission(p, permUtil.PERM_FEED_OTHER)) {
                        permUtil.returnNoPermission(p, permUtil.PERM_FEED_OTHER);
                        return true;
                    }

                    Player target = Bukkit.getPlayer(strings[0]);

                    if (target != null) {

                        target.setFoodLevel(20);
                        target.sendMessage(Data.getData().prefix + "§aDein Hunger wurde gestillt!");
                        p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §ahat nun keinen Hunger mehr!");

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/feed [Spieler]");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_FEED);

            }

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            if (strings.length == 0) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/feed <Spieler>");

            }

            else {

                Player target = Bukkit.getPlayer(strings[0]);

                if (target != null) {

                    target.setFoodLevel(20);
                    target.sendMessage(Data.getData().prefix + "§aDein Hunger wurde gestillt!");
                    p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §ahat nun keinen Hunger mehr!");
                    Data.getData().consoleIssuedNotify("Issued command /feed " + target.getName());

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                }

            }

        }

        return false;
    }
}
