package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class RandomTeleportCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (PermissionUtil.getUtil().hasPermission(p, PermissionUtil.getUtil().PERM_RANDOM_TELEPORT)) {

                int max = Bukkit.getOnlinePlayers().size();

                Random rnd = new Random();
                int random = rnd.nextInt(max);

                List<Player> online = new ArrayList<>(Bukkit.getOnlinePlayers());
                Player player = online.get(random);

                p.teleport(player.getLocation());
                p.sendMessage(Data.getData().prefix + "§aDu wurdest zu §b" + player.getName() + " §ateleportiert!");

            }

            else {

                PermissionUtil.getUtil().returnNoPermission(p, PermissionUtil.getUtil().PERM_RANDOM_TELEPORT);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
