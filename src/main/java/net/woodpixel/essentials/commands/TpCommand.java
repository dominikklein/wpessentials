package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.TeleportUtil;
import net.woodpixel.essentials.commandUtils.VanishHelper;
import net.woodpixel.essentials.configuration.TptoggleConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class TpCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_TP)) {

                if (strings.length == 1) {

                    Player target = Bukkit.getPlayer(strings[0]);

                    if (target != null) {

                        if (VanishHelper.getVanishHelper().isVanished(target) && !permUtil.hasPermission(p, permUtil.PERM_VANISH_INTERACT)) {
                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");
                            return true;
                        }

                        if (!TptoggleConfiguration.getConfiguration().isAllowed(target)) {
                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler verweigert die Teleportation!");
                            return true;
                        }

                        p.teleport(target.getLocation());
                        p.sendMessage(Data.getData().prefix + "§aDu wurdest teleportiert!");

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                    }

                }

                else if (strings.length == 2) {

                    if (!permUtil.hasPermission(p, permUtil.PERM_TPHERE)) {
                        permUtil.returnNoPermission(p, permUtil.PERM_TPHERE);
                        return true;
                    }

                    Player target = Bukkit.getPlayer(strings[0]);
                    Player target2 = Bukkit.getPlayer(strings[1]);

                    if (target != null && target2 != null) {

                        if (VanishHelper.getVanishHelper().isVanished(target) && !permUtil.hasPermission(p, permUtil.PERM_VANISH_INTERACT)) {
                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");
                            return true;
                        }

                        if (!TptoggleConfiguration.getConfiguration().isAllowed(target)) {
                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler verweigert die Teleportation!");
                            return true;
                        }

                        target.teleport(target2.getLocation());
                        p.sendMessage(Data.getData().prefix + "§b" + target.getName() + " §awurde zu §b" + target2.getName() + " §ateleportiert!");

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                    }

                }

                else if (strings.length == 3) {

                    if (!permUtil.hasPermission(p, permUtil.PERM_TPPOS)) {
                        permUtil.returnNoPermission(p, permUtil.PERM_TPPOS);
                        return true;
                    }

                    Location loc;

                    try {
                        int x = Integer.parseInt(strings[0]);
                        int y = Integer.parseInt(strings[1]);
                        int z = Integer.parseInt(strings[2]);
                        loc = new Location(p.getWorld(), x, y, z);
                    } catch (NumberFormatException e) {
                        p.sendMessage(Data.getData().prefix + "§cDiese Koordinaten sind ungültig!");
                        return true;
                    }

                    if (!TeleportUtil.getTeleportUtil().isWorldPermitted(p.getWorld().getName(), p, permUtil.PERM_TPPOS_PER_WORLD)) {
                        permUtil.returnNoPermission(p, permUtil.PERM_TPPOS_PER_WORLD + p.getWorld().getName());
                        return true;
                    }

                    p.teleport(loc);
                    p.sendMessage(Data.getData().prefix + "§aDu wurdest teleportiert!");

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/tp <Spieler>");

                }

            }

            else {

                permUtil.returnNoPermission(p, PermissionUtil.getUtil().PERM_TP);

            }

            return true;

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;

    }

}
