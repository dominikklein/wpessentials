package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class BroadcastCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (PermissionUtil.getUtil().hasPermission(p, PermissionUtil.getUtil().PERM_BROADCAST)) {

                if (strings.length == 0) {
                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/broadcast <Nachricht>");
                    return true;
                }

                String str = String.join(" ", strings);

                Bukkit.broadcastMessage("");
                Bukkit.broadcastMessage("");
                Bukkit.broadcastMessage(str.replaceAll("&", "§"));
                Bukkit.broadcastMessage("");
                Bukkit.broadcastMessage("");

            }

            else {

                PermissionUtil.getUtil().returnNoPermission(p, PermissionUtil.getUtil().PERM_BROADCAST);

            }

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender)commandSender;

            Data.getData().consoleIssuedNotify("Issued command /broadcast");

            if (strings.length == 0) {
                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/broadcast <Nachricht>");
                return true;
            }

            String str = String.join(" ", strings);

            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage(str.replaceAll("&", "§"));
            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage("");

        }

        return false;
    }
}
