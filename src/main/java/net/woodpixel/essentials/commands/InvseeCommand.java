package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.InventoryHelper;
import net.woodpixel.essentials.commandUtils.VanishHelper;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class InvseeCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_INVSEE)) {

                if (strings.length == 0) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/invsee <Spieler>");
                    return true;

                }

                else if (!permUtil.hasPermission(p, permUtil.PERM_INVSEE_MODIFY) &&
                        !permUtil.hasPermission(p, permUtil.PERM_INVSEE_ADMIN)) {

                    if (strings.length == 1) {

                        Player target = Bukkit.getPlayer(strings[0]);

                        if (target != null) {

                            if (VanishHelper.getVanishHelper().isVanished(target) && !permUtil.hasPermission(p, permUtil.PERM_VANISH_INTERACT)) {
                                p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist offline!");
                                return true;
                            }

                            InventoryHelper.getHelper().cancelInventoryActions(p);
                            p.openInventory(target.getInventory());

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist offline!");

                        }

                    }

                }

                else if (permUtil.hasPermission(p, permUtil.PERM_INVSEE_MODIFY)) {

                    if (strings.length == 1) {

                        Player target = Bukkit.getPlayer(strings[0]);

                        if (target != null) {

                            if (permUtil.hasPermission(target, permUtil.PERM_INVSEE_MODIFY_PREVENT) && !permUtil.hasPermission(p, permUtil.PERM_INVSEE_ADMIN)) {

                                InventoryHelper.getHelper().cancelInventoryActions(p);
                                p.openInventory(target.getInventory());

                            }

                            else if (!permUtil.hasPermission(target, permUtil.PERM_INVSEE_MODIFY_PREVENT)) {

                                p.openInventory(target.getInventory());

                            }

                            else if (permUtil.hasPermission(p, permUtil.PERM_INVSEE_ADMIN)) {

                                p.openInventory(target.getInventory());

                            }

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist offline!");

                        }

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/invsee <Spieler>");
                    return true;

                }

            }

            else {

                permUtil.returnNoPermission(p, PermissionUtil.getUtil().PERM_INVSEE);

            }

            return true;

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;

    }

}
