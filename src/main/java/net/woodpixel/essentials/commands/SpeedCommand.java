package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class SpeedCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_SPEED)) {

                if (strings.length == 0) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/speed <1-10> [Spieler]");
                    return true;

                }

                else if (strings.length == 1) {

                    if (strings[0].matches("[0-9]+")) {

                        int speed = Integer.parseInt(strings[0]);

                        float realSpeed = speed / 10.0F;

                        if (speed > 10 || speed < 1) {

                            p.sendMessage(Data.getData().prefix + "§cEs sind nur Werte von 1 bis 10 gültig!");

                        }

                        else {

                            if (p.isFlying()) {

                                p.setFlySpeed(realSpeed);
                                p.sendMessage(Data.getData().prefix + "§aDeine Fluggeschwindigkeit wurde auf §6" + speed + " §agesetzt!");

                            }

                            else {

                                p.setWalkSpeed(realSpeed);
                                p.sendMessage(Data.getData().prefix + "§aDeine Laufgeschwindigkeit wurde auf §6" + speed + " §agesetzt!");

                            }

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDie angegebene Zahl §6 " + strings[0] + " §cist ungültig");

                    }

                }

                else if (strings.length == 2) {

                    if (strings[0].matches("[0-9]+")) {

                        Player target = Bukkit.getPlayer(strings[1]);

                        if (target != null) {

                            if (!permUtil.hasPermission(p, permUtil.PERM_SPEED_OTHER)) {
                                permUtil.returnNoPermission(p, permUtil.PERM_SPEED_OTHER);
                                return true;
                            }

                            int speed = Integer.parseInt(strings[0]);

                            float realSpeed = speed / 10.0F;

                            if (speed > 10 || speed < 1) {

                                p.sendMessage(Data.getData().prefix + "§cEs sind nur Werte von 1 bis 10 gültig!");

                            }

                            else {

                                if (p.isFlying()) {

                                    target.setFlySpeed(realSpeed);
                                    target.sendMessage(Data.getData().prefix + "§aDeine Fluggeschwindigkeit wurde auf §6" + speed + " §agesetzt!");
                                    p.sendMessage(Data.getData().prefix + "§aDie Fluggeschwindigkeit von " + target.getDisplayName() +
                                            " §awurde auf §6" + speed + " §agesetzt!");

                                }

                                else {

                                    target.setWalkSpeed(realSpeed);
                                    target.sendMessage(Data.getData().prefix + "§aDeine Laufgeschwindigkeit wurde auf §6" + speed + " §agesetzt!");
                                    p.sendMessage(Data.getData().prefix + "§aDie Laufgeschwindigkeit von " + target.getDisplayName() +
                                            " §awurde auf §6" + speed + " §agesetzt!");

                                }

                            }

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                        }

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDie angegebene Zahl §6 " + strings[0] + " §cist ungültig");

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/speed <1-10> [Spieler]");
                    return true;

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_SPEED);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);
            return true;

        }

        return false;
    }
}
