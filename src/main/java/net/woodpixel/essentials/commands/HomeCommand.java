package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.HomeManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class HomeCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_HOME)) {

                if (strings.length == 0) {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/home <HomeName>");

                    if (HomeManager.getHomeManager().listHomes(p).isEmpty()) {
                        p.sendMessage(Data.getData().prefix + "§aHomes: §6-");
                    }

                    else {

                        List<String> homes = HomeManager.getHomeManager().listHomes(p);

                        String home = "";

                        for (int i = 0; i < homes.size(); i++) {

                            home += homes.get(i) + ", ";

                        }

                        home = home.substring(0, home.length() - 2);
                        p.sendMessage(Data.getData().prefix + "§aHomes: §6" + home);

                    }

                }

                else if (strings.length == 1) {

                    List<String> homes = HomeManager.getHomeManager().listHomes(p);

                    if (homes.contains(strings[0].toLowerCase())) {

                        if (HomeManager.getHomeManager().getHome(p, strings[0]).getWorld() == null) {
                            p.sendMessage(Data.getData().prefix + "§cDie Welt in der sich dein Home befindet wurde gelöscht!");
                            return true;
                        }

                        p.teleport(HomeManager.getHomeManager().getHome(p, strings[0]));
                        p.sendMessage(Data.getData().prefix + "§aDu wurdest zum Home §6" + strings[0].toLowerCase() + " §ateleportiert!");

                    }

                    else {

                        p.sendMessage(Data.getData().prefix + "§cDas Home §6" + strings[0] + " §cexistiert nicht!");
                        return true;

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/home <HomeName>");

                    if (HomeManager.getHomeManager().listHomes(p).isEmpty()) {
                        p.sendMessage(Data.getData().prefix + "§aHomes: §6-");
                    }

                    else {

                        List<String> homes = HomeManager.getHomeManager().listHomes(p);

                        String home = "";

                        for (int i = 0; i < homes.size(); i++) {

                            home += homes.get(i) + ", ";

                        }

                        home = home.substring(0, home.length() - 2);
                        p.sendMessage(Data.getData().prefix + "§aHomes: §6" + home);

                    }

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_HOME);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
