package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.event.BlockDamageEvents;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class GodCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_GOD)) {

                if (strings.length == 0) {

                    if (BlockDamageEvents.getDamageEvents().isGod(p)) {
                        BlockDamageEvents.getDamageEvents().unSetGod(p);
                        p.sendMessage(Data.getData().prefix + "§cDu bist nicht mehr unsterblich!");
                    }

                    else {
                        BlockDamageEvents.getDamageEvents().setGod(p);
                        p.sendMessage(Data.getData().prefix + "§aDu bist nun unsterblich!");
                    }

                }

                else if (strings.length == 1) {

                    if (permUtil.hasPermission(p, permUtil.PERM_GOD_OTHER)) {

                        Player target = Bukkit.getPlayer(strings[0]);

                        if (target != null) {

                            if (BlockDamageEvents.getDamageEvents().isGod(target)) {
                                BlockDamageEvents.getDamageEvents().unSetGod(target);
                                target.sendMessage(Data.getData().prefix + "§cDu bist nicht mehr unsterblich!");
                                p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §cist nicht mehr unsterblich!");
                            }

                            else {
                                BlockDamageEvents.getDamageEvents().setGod(target);
                                target.sendMessage(Data.getData().prefix + "§aDu bist nun unsterblich!");
                                p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §aist nun unsterblich!");
                            }

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                        }

                    }

                    else {

                        permUtil.returnNoPermission(p, permUtil.PERM_GOD_OTHER);

                    }

                }

                else {

                    p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/god [Spieler]");

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_GOD);

            }

            return true;

        }

        else {

            ConsoleCommandSender p = (ConsoleCommandSender) commandSender;

            if (strings.length == 0) {

                p.sendMessage(Data.getData().prefix + "§cVerwendung: §6/god [Spieler]");

            }

            else if (strings.length == 1) {

                Player target = Bukkit.getPlayer(strings[0]);

                if (target != null) {

                    if (BlockDamageEvents.getDamageEvents().isGod(target)) {
                        BlockDamageEvents.getDamageEvents().unSetGod(target);
                        target.sendMessage(Data.getData().prefix + "§cDu bist nicht mehr unsterblich!");
                        p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §cist nicht mehr unsterblich!");
                    } else {
                        BlockDamageEvents.getDamageEvents().setGod(target);
                        target.sendMessage(Data.getData().prefix + "§aDu bist nun unsterblich!");
                        p.sendMessage(Data.getData().prefix + target.getDisplayName() + " §aist nun unsterblich!");
                    }

                    Data.getData().consoleIssuedNotify("Issued command /god " + target.getName());

                } else {

                    p.sendMessage(Data.getData().prefix + "§cDieser Spieler ist nicht online!");

                }

            }

        }

        return false;

    }
}
