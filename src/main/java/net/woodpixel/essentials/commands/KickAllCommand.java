package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class KickAllCommand implements CommandExecutor {

    private List<Player> confirm = new ArrayList<>();
    private List<ConsoleCommandSender> confirmC = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player player = (Player)commandSender;

            if (PermissionUtil.getUtil().hasPermission(player, PermissionUtil.getUtil().PERM_KICK_ALL)) {

                if (strings.length == 0) {

                    confirm.add(player);
                    player.sendMessage(Data.getData().prefix + "§cBitte bestätige das du alle Spieler kicken möchtest " +
                            "mit §e/kickall confirm§c!");

                }

                else if (strings[0].equalsIgnoreCase("confirm")) {

                    if (!confirm.contains(player)) {
                        player.sendMessage(Data.getData().prefix + "§cDu hast nicht zu bestätigen!");
                        return true;
                    }

                    for (Player all : Bukkit.getOnlinePlayers()) {
                        if (!all.equals(player)) {
                            all.kickPlayer("Vom Server geworfen...");
                        }
                    }

                    player.sendMessage(Data.getData().prefix + "§aEs wurden alle Spieler gekickt!");

                }

                else {

                    player.performCommand("kickall");

                }

            }

            else {

                PermissionUtil.getUtil().returnNoPermission(player, PermissionUtil.getUtil().PERM_KICK_ALL);

            }

        }

        else {

            ConsoleCommandSender player = (ConsoleCommandSender)commandSender;

            if (strings.length == 0) {

                confirmC.add(player);
                player.sendMessage(Data.getData().prefix + "§cBitte bestätige das du alle Spieler kicken möchtest " +
                        "mit §e/kickall confirm§c!");

            }

            else if (strings[0].equalsIgnoreCase("confirm")) {

                if (!confirmC.contains(player)) {
                    player.sendMessage(Data.getData().prefix + "§cDu hast nicht zu bestätigen!");
                    return true;
                }

                for (Player all : Bukkit.getOnlinePlayers()) {
                    if (!all.equals(player)) {
                        all.kickPlayer("Vom Server geworfen...");
                    }
                }

                player.sendMessage(Data.getData().prefix + "§aEs wurden alle Spieler gekickt!");
                Data.getData().consoleIssuedNotify("Issued command /kickall");

            }

            else {

                return true;

            }

        }

        return false;
    }
}
