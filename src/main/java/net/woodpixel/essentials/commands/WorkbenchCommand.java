package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class WorkbenchCommand implements CommandExecutor {

    private PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player player = (Player)commandSender;

            if (permUtil.hasPermission(player, permUtil.PERM_WORKBENCH)) {

                player.openWorkbench(player.getLocation(), true);

            }

            else {

                permUtil.returnNoPermission(player, permUtil.PERM_WORKBENCH);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
