package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.commandUtils.AdminInvUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class AdminInvCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player player = (Player)commandSender;
            PermissionUtil perm = PermissionUtil.getUtil();

            if (perm.hasPermission(player, perm.PERM_ADMIN_INV)) {

                if (strings.length == 0) {

                    AdminInvUtils.getUtils().loadInventory(player, player, 1);

                }

                else {

                    if (!perm.hasPermission(player, perm.PERM_ADMIN_INV_OTHER)) {
                        perm.returnNoPermission(player, perm.PERM_ADMIN_INV_OTHER);
                        return true;
                    }

                    OfflinePlayer player1 = Bukkit.getOfflinePlayer(strings[0]);

                    if (AdminInvUtils.getUtils().existsPlayer(player1)) {
                        AdminInvUtils.getUtils().loadInventory(player1, player, 1);
                    }

                    else {
                        player.sendMessage(Data.getData().prefix + "§cDer Spieler §6" + strings[0] + " §cwurde nicht gefunden!");
                    }

                }

            }

            else {

                perm.returnNoPermission(player, perm.PERM_ADMIN_INV);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }
}
