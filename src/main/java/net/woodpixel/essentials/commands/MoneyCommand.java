package net.woodpixel.essentials.commands;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.eco.Economy;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class MoneyCommand implements CommandExecutor {

    PermissionUtil permUtil = PermissionUtil.getUtil();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {

            Player p = (Player)commandSender;

            if (permUtil.hasPermission(p, permUtil.PERM_MONEY)) {

                if (strings.length == 0) {

                    p.sendMessage(Data.getData().prefix + "§aKontostand: §6" + format(Economy.getEco().getMoney(p)) + "$");
                    return true;

                }

                else if (strings.length >= 1) {

                    if (permUtil.hasPermission(p, permUtil.PERM_MONEY_OTHER)) {

                        OfflinePlayer target = Bukkit.getOfflinePlayer(strings[0]);

                        if (target.hasPlayedBefore() && target != null) {

                            p.sendMessage(Data.getData().prefix + "§aDer Kontostand von §b" + target.getName() + " §abeträgt: §6" + format(Economy.getEco().getMoney(target)) + "$");

                        }

                        else {

                            p.sendMessage(Data.getData().prefix + "§cDieser Spieler existiert nicht!");

                        }

                    }

                    else {

                        permUtil.returnNoPermission(p, permUtil.PERM_MONEY_OTHER);

                    }

                }

            }

            else {

                permUtil.returnNoPermission(p, permUtil.PERM_MONEY);

            }

        }

        else {

            Data.getData().returnNoConsoleCommand(commandSender, command);

        }

        return false;
    }

    private String format(double amount) {

        NumberFormat nf = NumberFormat.getInstance(new Locale("de", "DE"));
        return nf.format(amount);

    }

}
