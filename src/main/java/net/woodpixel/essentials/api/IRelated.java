package net.woodpixel.essentials.api;

import net.woodpixel.essentials.configuration.PluginState;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public interface IRelated {

    PluginState getPluginState();

    void setPluginState(PluginState pluginState);

}
