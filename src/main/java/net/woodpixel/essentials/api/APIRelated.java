package net.woodpixel.essentials.api;

import net.woodpixel.essentials.Registerer;
import net.woodpixel.essentials.configuration.PluginState;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class APIRelated implements IRelated {

    private static APIRelated apiRelated;

    public APIRelated() {
        apiRelated = this;
    }

    public static APIRelated getApiRelated() {
        return apiRelated;
    }

    /**
     * start of Interface
     */

    @Override
    public PluginState getPluginState() {
        return Registerer.getState();
    }

    @Override
    public void setPluginState(PluginState pluginState) {
        Registerer.setState(pluginState);
    }



}
