package net.woodpixel.essentials.api;

import net.woodpixel.essentials.BaseProvider;
import net.woodpixel.essentials.commandUtils.VanishHelper;
import net.woodpixel.essentials.eco.Economy;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class APIProvider {

    /**
     * API providing Methods
     */

    private static APIProvider apiProvider;
    private APIRelated related = APIRelated.getApiRelated();
    private VanishHelper vanishHelper = VanishHelper.getVanishHelper();
    private Economy economy = Economy.getEco();

    public enum VanishValue {
        ADMIN, TEAM
    }

    public APIProvider() {
        apiProvider = this;
    }

    public static APIProvider getApiProvider() {
        return apiProvider;
    }

    /**
     * API provided Methods for public easy use
     */



    public boolean getVanishState(Player p) {
        return vanishHelper.isVanished(p);
    }

    /**
     * vanish player - set Player invisible
     * @param p
     * @param vanishValue
     */

    public void vanishPlayer(Player p, VanishValue vanishValue) {

        String value = null;
        if (vanishValue == VanishValue.ADMIN) {
            value = "admin";
        }
        else if (vanishValue == VanishValue.TEAM) {
            value = "team";
        }

        vanishHelper.vanishPlayer(p, value);
    }

    /**
     * unvanish player - set player visible
     * @param p
     */

    public void unVanishPlayer(Player p) {
        vanishHelper.unvanishPlayer(p);
    }

    /**
     * Get the money of specified player
     * @param p
     * @return double value of money
     */

    public double getMoney(Player p) {
        return economy.getMoney(p);
    }

    /**
     * remove specified amount of money to specified player
     * @param p
     * @param money
     */

    public void removeMoney(Player p, double money) {
        economy.removeMoney(p, money);
    }

    /**
     * add specified amount of money to specified player
     * @param p
     * @param money
     */

    public void addMoney(Player p, double money) {
        economy.addMoney(p, money);
    }

    /**
     * set specified amount of money to specified player
     * @param p
     * @param money
     */

    public void setMoney(Player p, double money) {
        economy.setPlayersMoney(p, money);
    }

    /**
     * get related Methods Class
     * @return
     */

    public APIRelated getRelated() {
        return this.related;
    }

    public boolean isNewPlayer(Player player) {
        return BaseProvider.getBaseProvider().existsBank(player);
    }



}
