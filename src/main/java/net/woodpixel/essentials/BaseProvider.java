package net.woodpixel.essentials;

import net.woodpixel.essentials.eco.Economy;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.*;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class BaseProvider {

    private static BaseProvider baseProvider;
    private JavaPlugin javaPlugin;
    private final String host, username, password, database, databaseEco, tablePrefix, tableEco;
    private final int port;
    private Connection connection;
    private Connection bankConnection;
    private final String DB_USER;

    public BaseProvider(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        baseProvider = this;

        this.host = javaPlugin.getConfig().getString("mysql.host");
        this.username = javaPlugin.getConfig().getString("mysql.user");
        this.password = javaPlugin.getConfig().getString("mysql.password");
        this.database = javaPlugin.getConfig().getString("mysql.database");
        this.databaseEco = javaPlugin.getConfig().getString("mysql.databaseEco");
        this.tablePrefix = javaPlugin.getConfig().getString("mysql.tablePrefix");
        this.tableEco = javaPlugin.getConfig().getString("mysql.bankTable");
        this.port = javaPlugin.getConfig().getInt("mysql.port");

        this.DB_USER = this.tablePrefix + "userFile";

        openConnectionBank();
        openConnection();

    }

    public static BaseProvider getBaseProvider() {
        return baseProvider;
    }

    public boolean isClosedBank() throws SQLException {
        return this.bankConnection == null || this.bankConnection.isClosed() || !this.bankConnection.isValid(0);
    }

    public void openConnectionBank() {

        try {

            if (isClosedBank()) {
                Class.forName("com.mysql.jdbc.Driver");
                this.bankConnection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.databaseEco,
                        this.username, this.password);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void executeBank(String sql) {
        try {
            if (isClosedBank()) {
                openConnectionBank();
            }

            Statement statement = this.bankConnection.createStatement();
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isClosed() throws SQLException {
        return this.connection == null || this.connection.isClosed() || !this.connection.isValid(0);
    }

    public void openConnection() {

        try {

            if (isClosed()) {
                Class.forName("com.mysql.jdbc.Driver");
                this.connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database,
                        this.username, this.password);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void execute(String sql) {
        try {
            if (isClosed()) {
                openConnection();
            }

            Statement statement = this.connection.createStatement();
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * BANK METHODS
     */

    public boolean existsBank(OfflinePlayer player) {

        openConnectionBank();

        String sql = "select * from " + this.tableEco + " where player_uuid = '" + player.getUniqueId() + "';";
        boolean b = false;

        try {

            Statement statement = this.bankConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            return resultSet.next();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;

    }

    public void addPlayerElseUpdate(Player player) {

        if (!existsBank(player)) {

            String sql = "insert into " + this.tableEco + " (player_uuid, player_name, money, offline_money, sync_complete, last_seen)" +
                    " values ('" + player.getUniqueId().toString() + "', '" + player.getName() + "', 0, 0, true, '" + String.valueOf(System.currentTimeMillis()) + "');";
            executeBank(sql);

        }

        else {

            String sql = "update " + this.tableEco + " set player_name = '" + player.getName() + "'" +
                    " where player_uuid = '" + player.getUniqueId() + "';";
            executeBank(sql);

        }

    }

    public void uploadPlayer(Player player) {

        String sql = "update " + this.tableEco + " set money = '" + Economy.getEco().getMoney(player) + "', sync_complete = true, " +
                "last_seen = '" + System.currentTimeMillis() + "' where player_uuid = '" + player.getUniqueId() + "';";
        executeBank(sql);

    }

    public void downloadPlayer(Player player) {

        openConnectionBank();

        String sql = "select * from " + this.tableEco + " where player_uuid = '" + player.getUniqueId() + "';";

        try {

            Statement statement = this.bankConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                Economy.getEco().setPlayersMoney(player, resultSet.getDouble("money"));

            }

            statement.close();
            resultSet.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void setCompleted(Player player) {

        String sql = "update " + this.tableEco + " set sync_complete = true where player_uuid = '" + player.getUniqueId() + "';";
        executeBank(sql);

    }

    public boolean isCompleted(Player player) {

        openConnectionBank();

        String sql = "select * from " + this.tableEco + " where player_uuid = '" + player.getUniqueId() + "';";
        boolean b = false;

        try {

            Statement statement = this.bankConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                if (resultSet.getString("sync_complete").equalsIgnoreCase("true")) {
                    b = true;
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return b;

    }

}
