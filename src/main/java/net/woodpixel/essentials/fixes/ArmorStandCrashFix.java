package net.woodpixel.essentials.fixes;

import net.woodpixel.essentials.Main;
import org.bukkit.entity.ArmorStand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ArmorStandCrashFix implements Listener {

    @EventHandler
    public void onPlace(EntitySpawnEvent event) {

        if (event.getEntity() instanceof ArmorStand) {

            ArmorStand armorStand = (ArmorStand) event.getEntity();
            armorStand.setGravity(false);
            armorStand.setVelocity(null);
            if (!Main.version.equalsIgnoreCase("v1_8_R3")) {
                armorStand.setCollidable(false);
            }

        }

    }

}
