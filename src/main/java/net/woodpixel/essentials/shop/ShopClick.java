package net.woodpixel.essentials.shop;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public enum ShopClick {

    PERMS, EAT, NORMAL, RARE, EXPENSIVE, CLOSE, BACK

}
