package net.woodpixel.essentials.shop;

import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.Node;
import me.lucko.luckperms.api.User;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.woodpixel.essentials.Main;
import net.woodpixel.essentials.eco.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.RegisteredServiceProvider;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class Shop implements IShop {

    private static Shop shop;
    private LuckPermsApi luckPermsApi;

    public Shop() {
        shop = this;
        RegisteredServiceProvider<LuckPermsApi> provider = Bukkit.getServicesManager().getRegistration(LuckPermsApi.class);
        if (provider != null) {
            luckPermsApi = provider.getProvider();
        }
    }

    public static Shop getShop() {
        return shop;
    }

    @Override
    public boolean hasEnoughMoney(Player p, double price) {
        if (Economy.getEco().getMoney(p) >= price) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isSpaceAvailable(Player player) {

        Inventory inventory = player.getInventory();
        if (inventory.firstEmpty() == -1) {
            return false;
        }
        return true;

    }

    @Override
    public void addPermission(Player player, String permissionNode) {
        User user = luckPermsApi.getUser(player.getUniqueId());
        Node node = luckPermsApi.getNodeFactory().newBuilder(permissionNode).setValue(true).build();
        luckPermsApi.getUser(user.getUuid()).setPermission(node);
        luckPermsApi.getUserManager().saveUser(user);
    }

    @Override
    public void spawnShopNPC(Location location) {

        Villager villager = (Villager) location.getWorld().spawnEntity(location, EntityType.VILLAGER);
        villager.setCustomName("§b✢§cAdmin§eShop§b✢");
        villager.setCustomNameVisible(true);

        if (Main.version.equalsIgnoreCase("v1_8_R3")) {
            net.minecraft.server.v1_8_R3.Entity nmsEn = ((CraftEntity) villager).getHandle();
            NBTTagCompound compound = new NBTTagCompound();
            nmsEn.c(compound);
            compound.setByte("NoAI", (byte) 1);
            nmsEn.f(compound);
        }
        else if (Main.version.equalsIgnoreCase("v1_12_R1")) {
            net.minecraft.server.v1_12_R1.Entity nmsEn = ((org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity) villager).getHandle();
            net.minecraft.server.v1_12_R1.NBTTagCompound compound = new net.minecraft.server.v1_12_R1.NBTTagCompound();
            nmsEn.c(compound);
            compound.setByte("NoAI", (byte) 1);
            nmsEn.f(compound);
        }

    }
}
