package net.woodpixel.essentials.shop;

import net.woodpixel.essentials.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.*;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ShopConfigurator {

    private static ShopConfigurator shopConfigurator;
    public static Inventory iMain = Bukkit.createInventory(null, 27, "§6Shop");
    public Inventory iNormal = Bukkit.createInventory(null, 36, "§6Shop");
    public Inventory iPerms = Bukkit.createInventory(null, 45, "§6Shop");
    public Inventory iRare = Bukkit.createInventory(null, 27, "§6Shop");
    public Inventory iEat = Bukkit.createInventory(null, 27, "§6Shop");
    public Inventory iExpensive = Bukkit.createInventory(null, 27, "§6Shop");

    public HashMap<ItemStack, Double> itemPriceMap = new HashMap<>();
    public HashMap<ItemStack, String> permSectionName = new HashMap<>();
    public static HashMap<ItemStack, ShopClick> mainInvClickedItem = new HashMap<>();

    public ShopConfigurator() {
        shopConfigurator = this;
        addItemsConfig();
        loadItems();
    }

    public static ShopConfigurator getShopConfigurator() {
        return shopConfigurator;
    }

    static {

        ItemStack permissions = itemBuilder("§eRechte", "§chier kannst du Perks und rechte erwerben!",
                                Material.BOOK, (short) 0, 1);
        ItemStack normal = itemBuilder("§7normale Items", "§chier kannst du normale Items erwerben!",
                            Material.WOOD, (short) 0, 1);
        ItemStack rare = itemBuilder("§bseltene Items", "§chier kannst du seltene Items erwerben!",
                            Material.BEACON, (short) 0, 1);
        ItemStack eat = itemBuilder("§aEssen", "§chier kannst du Essen kaufen!", Material.COOKED_BEEF,
                        (short) 0, 1);
        ItemStack expensive = itemBuilder("§dsehr seltene Items", "§chier kannst du seltene und teure Items kaufen",
                        Material.MOB_SPAWNER, (short) 0, 1);

        ItemStack close = itemBuilder("§cSchließen", null, Material.IRON_DOOR, (short) 0, 1);

        iMain.setItem(9, permissions);
        mainInvClickedItem.put(permissions, ShopClick.PERMS);

        iMain.setItem(11, eat);
        mainInvClickedItem.put(eat, ShopClick.EAT);

        iMain.setItem(13, normal);
        mainInvClickedItem.put(normal, ShopClick.NORMAL);

        iMain.setItem(15, rare);
        mainInvClickedItem.put(rare, ShopClick.RARE);

        iMain.setItem(17, expensive);
        mainInvClickedItem.put(expensive, ShopClick.EXPENSIVE);

        iMain.setItem(26, close);
        mainInvClickedItem.put(close, ShopClick.CLOSE);

    }

    public void addItemsConfig() {

        File file = new File(Main.getInstance().getDataFolder(), "shopItems.yml");

        if (!file.exists()) {
            try {
                file.createNewFile();
                YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

                String starterNormal = "items.normal";
                String starterPerms = "items.perms";
                String starterRare = "items.selten";
                String starterEat = "items.eat";
                String starterExpensive = "items.wertvoll";

                config.addDefault(starterNormal + ".kessel" + ".itemId", "380");
                config.addDefault(starterNormal + ".kessel" + ".itemSubId", "0");
                config.addDefault(starterNormal + ".kessel" + ".price", "30");
                config.addDefault(starterNormal + ".kessel" + ".amount", "1");
                config.addDefault(starterNormal + ".kessel" + ".slot", "1");

                config.addDefault(starterPerms + ".kessel" + ".itemId", "380");
                config.addDefault(starterPerms + ".kessel" + ".itemSubId", "0");
                config.addDefault(starterPerms + ".kessel" + ".name", "KesselPermission");
                config.addDefault(starterPerms + ".kessel" + ".price", "30");
                config.addDefault(starterPerms + ".kessel" + ".slot", "1");
                config.addDefault(starterPerms + ".kessel" + ".permission", "perm.kessel");

                config.addDefault(starterRare + ".kessel" + ".itemId", "380");
                config.addDefault(starterRare + ".kessel" + ".itemSubId", "0");
                config.addDefault(starterRare + ".kessel" + ".price", "30");
                config.addDefault(starterRare + ".kessel" + ".amount", "1");
                config.addDefault(starterRare + ".kessel" + ".slot", "1");

                config.addDefault(starterEat + ".kessel" + ".itemId", "380");
                config.addDefault(starterEat + ".kessel" + ".itemSubId", "0");
                config.addDefault(starterEat + ".kessel" + ".price", "30");
                config.addDefault(starterEat + ".kessel" + ".amount", "1");
                config.addDefault(starterEat + ".kessel" + ".slot", "1");

                config.addDefault(starterExpensive + ".kessel" + ".itemId", "380");
                config.addDefault(starterExpensive + ".kessel" + ".itemSubId", "0");
                config.addDefault(starterExpensive + ".kessel" + ".price", "30");
                config.addDefault(starterExpensive + ".kessel" + ".amount", "1");
                config.addDefault(starterExpensive + ".kessel" + ".slot", "1");

                config.options().copyDefaults(true);

                config.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void loadItems() {

        File file = new File(Main.getInstance().getDataFolder(), "shopItems.yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

        List<String> normal = new ArrayList<>();
        List<String> perms = new ArrayList<>();
        List<String> rare = new ArrayList<>();
        List<String> eat = new ArrayList<>();
        List<String> expensive = new ArrayList<>();

        ConfigurationSection sectionNormal = config.getConfigurationSection("items.normal");
        ConfigurationSection sectionPerms = config.getConfigurationSection("items.perms");
        ConfigurationSection sectionRare = config.getConfigurationSection("items.selten");
        ConfigurationSection sectionEat = config.getConfigurationSection("items.eat");
        ConfigurationSection sectionExpensive = config.getConfigurationSection("items.wertvoll");

        Set<String> lNormal = sectionNormal.getKeys(false);
        Set<String> lPerms = sectionPerms.getKeys(false);
        Set<String> lRare = sectionRare.getKeys(false);
        Set<String> lEat = sectionEat.getKeys(false);
        Set<String> lExpensive = sectionExpensive.getKeys(false);

        if (!lNormal.isEmpty()) {
            for (String s : lNormal) {
                normal.add(s);
            }
        }

        if (!lPerms.isEmpty()) {
            for (String s : lPerms) {
                perms.add(s);
            }
        }

        if (!lRare.isEmpty()) {
            for (String s : lRare) {
                rare.add(s);
            }
        }

        if (!lEat.isEmpty()) {
            for (String s : lEat) {
                eat.add(s);
            }
        }

        if (!lExpensive.isEmpty()) {
            for (String s : lExpensive) {
                expensive.add(s);
            }
        }

        String starterNormal = "items.normal.";
        String starterPerms = "items.perms.";
        String starterRare = "items.selten.";
        String starterEat = "items.eat.";
        String starterExpensive = "items.wertvoll.";

        for (String s : normal) {

            int id = Integer.parseInt(config.getString(starterNormal + s + ".itemId"));
            int amount = Integer.parseInt(config.getString(starterNormal + s + ".amount"));
            String subId = config.getString(starterNormal + s + ".itemSubId");
            ItemStack is = new ItemStack(Material.getMaterial(id), amount, Short.parseShort(subId));
            ItemMeta im = is.getItemMeta();
            im.setLore(Arrays.asList("", "§cPreis: §b" + format(Integer.parseInt(config.getString(starterNormal + s + ".price"))) + "$"));
            is.setItemMeta(im);
            iNormal.setItem(Integer.parseInt(config.getString(starterNormal + s + ".slot")), is);
            itemPriceMap.put(is, Double.parseDouble(config.getString(starterNormal + s + ".price")));

        }

        for (String s : perms) {

            int id = Integer.parseInt(config.getString(starterPerms + s + ".itemId"));
            String subId = config.getString(starterPerms + s + ".itemSubId");
            ItemStack is = new ItemStack(Material.getMaterial(id), 1, Short.parseShort(subId));
            ItemMeta im = is.getItemMeta();
            im.setDisplayName(config.getString(starterPerms + s + ".name"));
            im.setLore(Arrays.asList("", "§cPreis: §b" + format(Integer.parseInt(config.getString(starterPerms + s + ".price"))) + "$"));
            is.setItemMeta(im);
            iPerms.setItem(Integer.parseInt(config.getString(starterPerms + s + ".slot")), is);
            itemPriceMap.put(is, Double.parseDouble(config.getString(starterPerms + s + ".price")));
            permSectionName.put(is, s);

        }

        for (String s : rare) {

            int id = Integer.parseInt(config.getString(starterRare + s + ".itemId"));
            int amount = Integer.parseInt(config.getString(starterRare + s + ".amount"));
            String subId = config.getString(starterRare + s + ".itemSubId");
            ItemStack is = new ItemStack(Material.getMaterial(id), amount, Short.parseShort(subId));
            ItemMeta im = is.getItemMeta();
            im.setLore(Arrays.asList("", "§cPreis: §b" + format(Integer.parseInt(config.getString(starterRare + s + ".price"))) + "$"));
            is.setItemMeta(im);
            iRare.setItem(Integer.parseInt(config.getString(starterRare + s + ".slot")), is);
            itemPriceMap.put(is, Double.parseDouble(config.getString(starterRare + s + ".price")));

        }

        for (String s : eat) {

            int id = Integer.parseInt(config.getString(starterEat + s + ".itemId"));
            int amount = Integer.parseInt(config.getString(starterEat + s + ".amount"));
            String subId = config.getString(starterEat + s + ".itemSubId");
            ItemStack is = new ItemStack(Material.getMaterial(id), amount, Short.parseShort(subId));
            ItemMeta im = is.getItemMeta();
            im.setLore(Arrays.asList("", "§cPreis: §b" + format(Integer.parseInt(config.getString(starterEat + s + ".price"))) + "$"));
            is.setItemMeta(im);
            iEat.setItem(Integer.parseInt(config.getString(starterEat + s + ".slot")), is);
            itemPriceMap.put(is, Double.parseDouble(config.getString(starterEat + s + ".price")));

        }

        for (String s : expensive) {

            int id = Integer.parseInt(config.getString(starterExpensive + s + ".itemId"));
            int amount = Integer.parseInt(config.getString(starterExpensive + s + ".amount"));
            String subId = config.getString(starterExpensive + s + ".itemSubId");
            ItemStack is = new ItemStack(Material.getMaterial(id), amount, Short.parseShort(subId));
            ItemMeta im = is.getItemMeta();
            im.setLore(Arrays.asList("", "§cPreis: §b" + format(Integer.parseInt(config.getString(starterExpensive + s + ".price"))) + "$"));
            is.setItemMeta(im);
            iExpensive.setItem(Integer.parseInt(config.getString(starterExpensive + s + ".slot")), is);
            itemPriceMap.put(is, Double.parseDouble(config.getString(starterExpensive + s + ".price")));

        }

        ItemStack close = itemBuilder("§cSchließen", null, Material.IRON_DOOR, (short) 0, 1);
        ItemStack back = itemBuilder("§cZurück", null, Material.getMaterial(431), (short) 0, 1);

        iEat.setItem(26, back);
        iEat.setItem(22, close);

        iNormal.setItem(35, back);
        iNormal.setItem(31, close);

        iRare.setItem(26, back);
        iRare.setItem(22, close);

        iExpensive.setItem(26, back);
        iExpensive.setItem(22, close);

        iPerms.setItem(44, back);
        iPerms.setItem(40, close);
        mainInvClickedItem.put(back, ShopClick.BACK);

    }

    private static ItemStack itemBuilder(String display, String lore, Material material, short sub, int amount) {

        ItemStack is = new ItemStack(material, amount, sub);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(display);
        im.setLore(Arrays.asList("", lore));
        is.setItemMeta(im);
        return is;

    }

    private String format(double amount) {

        NumberFormat nf = NumberFormat.getInstance(new Locale("de", "DE"));
        return nf.format(amount);

    }

}
