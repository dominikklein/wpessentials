package net.woodpixel.essentials.shop;

import net.woodpixel.essentials.Main;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ShopSoundHandler {

    private static ShopSoundHandler soundHandler;

    public ShopSoundHandler() {
        soundHandler = this;
    }

    public static ShopSoundHandler getHandler() {
        return soundHandler;
    }

    public void playSoundFailed(Player player) {
        if (Main.version.equalsIgnoreCase("v1_8_R3")) {
            player.playSound(player.getLocation(), Sound.valueOf("NOTE_PIANO"), 2.0F, 2.0F);
        }
        else {
            player.playSound(player.getLocation(), Sound.valueOf("BLOCK_NOTE_PLING"), 2.0F, 2.0F);
        }
    }

    public void playSoundSuccess(Player player) {
        if (Main.version.equalsIgnoreCase("v1_8_R3")) {
            player.playSound(player.getLocation(), Sound.valueOf("LEVEL_UP"), 2.0F, 2.0F);
        }
        else {
            player.playSound(player.getLocation(), Sound.valueOf("ENTITY_PLAYER_LEVELUP"), 2.0F, 2.0F);
        }
    }

}
