package net.woodpixel.essentials.shop;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public interface IShop {

    boolean hasEnoughMoney(Player p, double price);

    boolean isSpaceAvailable(Player player);

    void addPermission(Player player, String permissionNode);

    void spawnShopNPC(Location location);

}
