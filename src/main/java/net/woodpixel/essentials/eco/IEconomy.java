package net.woodpixel.essentials.eco;

import org.bukkit.OfflinePlayer;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public interface IEconomy {

    /**
     * @param p
     * @returns Players Money
     */
    double getMoney(OfflinePlayer p);

    /**
     * @param p
     * @param d
     * Sets players money
     */
    void setPlayersMoney(OfflinePlayer p, double d);

    void addMoney(OfflinePlayer p, double d);

    void removeMoney(OfflinePlayer p, double d);

    boolean hasEnoughMoney(OfflinePlayer p, double d);

}
