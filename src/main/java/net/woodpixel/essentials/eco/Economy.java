package net.woodpixel.essentials.eco;

import net.woodpixel.essentials.configuration.PlayerDataInstance;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.IOException;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class Economy implements IEconomy {

    private static Economy eco;

    public static Economy getEco() {
        return eco;
    }

    public Economy() {
        eco = this;
    }

    /**
     * Overrides
     */

    @Override
    public double getMoney(OfflinePlayer p) {
        YamlConfiguration config = PlayerDataInstance.getpDI().getPlayerConfig(p);
        if (config.get("eco.balance") == null) {
            return 0;
        }
        else {
            return config.getDouble("eco.balance");
        }
    }

    @Override
    public void setPlayersMoney(OfflinePlayer p, double d) {
        YamlConfiguration config = PlayerDataInstance.getpDI().getPlayerConfig(p);
        config.set("eco.balance", d);
        try {
            config.save(PlayerDataInstance.getpDI().getPlayerFile(p));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addMoney(OfflinePlayer p, double d) {
        YamlConfiguration config = PlayerDataInstance.getpDI().getPlayerConfig(p);
        if (config.get("eco.balance") == null) {
            setPlayersMoney(p, d);
        }
        else {
            double money = getMoney(p);
            double newMoney = money + d;
            setPlayersMoney(p, newMoney);
        }
    }

    @Override
    public void removeMoney(OfflinePlayer p, double d) {
        YamlConfiguration config = PlayerDataInstance.getpDI().getPlayerConfig(p);
        if (config.getString("eco.balance") == null) {
            setPlayersMoney(p, 0 - d);
        }
        else {
            double money = getMoney(p);
            double newMoney = money - d;
            setPlayersMoney(p, newMoney);
        }
    }

    @Override
    public boolean hasEnoughMoney(OfflinePlayer p, double d) {

        if (getMoney(p) < d) {
            return false;
        }
        else if (getMoney(p) >= d) {
            return true;
        }

        return false;
    }
}
