package net.woodpixel.essentials;

import net.woodpixel.essentials.antidupe.DupeAPI;
import net.woodpixel.essentials.api.APIProvider;
import net.woodpixel.essentials.api.APIRelated;
import net.woodpixel.essentials.commandUtils.*;
import net.woodpixel.essentials.commands.*;
import net.woodpixel.essentials.configuration.*;
import net.woodpixel.essentials.event.*;
import net.woodpixel.essentials.fixes.ArmorStandCrashFix;
import net.woodpixel.essentials.shop.Shop;
import net.woodpixel.essentials.shop.ShopConfigurator;
import net.woodpixel.essentials.shop.ShopSoundHandler;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class Registerer {

    private JavaPlugin javaPlugin;
    private static PluginState pluginState;

    public Registerer(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        registerAll();
    }

    public void registerAll() {

        /**
         * RELATED
         */

        BlockDamageEvents blockDamageEvents = new BlockDamageEvents(javaPlugin);
        blockDamageEvents.setDamageEvents(); sendInitializer(BlockDamageEvents.class);

        SpawnProvider spawnProvider = new SpawnProvider(javaPlugin);
        spawnProvider.loadSpawnLocation(); sendInitializer(SpawnProvider.class);

        TeleportUtil teleportUtil = new TeleportUtil(javaPlugin);
        teleportUtil.setTeleportUtil(); sendInitializer(TeleportUtil.class);

        new TptoggleConfiguration(javaPlugin).setConfiguration(); sendInitializer(TptoggleConfiguration.class);
        new InventoryHelper(javaPlugin).setInventory(); sendInitializer(InventoryHelper.class);
        new PermissionUtil(); sendInitializer(PermissionUtil.class);
        new PlayerOptions(javaPlugin); sendInitializer(PlayerOptions.class);
        new DupeAPI(javaPlugin); sendInitializer(DupeAPI.class);
        new MainJoinEvent(javaPlugin).setJoinEvent(); sendInitializer(MainJoinEvent.class);
        new MainQuitEvent(javaPlugin).setJoinEvent(); sendInitializer(MainQuitEvent.class);
        new PlayerDataInstance(); PlayerDataInstance.setupDataInstance(); sendInitializer(PlayerDataInstance.class);
        new net.woodpixel.essentials.eco.Economy(); sendInitializer(net.woodpixel.essentials.eco.Economy.class);
        new PlayerData(); sendInitializer(PlayerData.class);
        new Warp(javaPlugin); sendInitializer(Warp.class);
        new HomeManager(); sendInitializer(HomeManager.class);
        new APIProvider(); sendInitializer(APIProvider.class);
        new APIRelated(); sendInitializer(APIRelated.class);
        new MainRespawnEvent(javaPlugin); sendInitializer(MainRespawnEvent.class);
        new VanishHelper(javaPlugin); sendInitializer(VanishHelper.class);
        new BaseProvider(javaPlugin); sendInitializer(BaseProvider.class);
        new SyncListener(javaPlugin); sendInitializer(SyncListener.class);
        new ItemAdaptor(); sendInitializer(ItemAdaptor.class);
        new FontHandler(); sendInitializer(FontHandler.class);
        new ShopConfigurator(); sendInitializer(ShopConfigurator.class);
        new Shop(); sendInitializer(Shop.class);
        new ShopSoundHandler(); sendInitializer(ShopSoundHandler.class);
        new RegisterUnbreakable(); sendInitializer(RegisterUnbreakable.class);
        new AdminInvUtils(javaPlugin); sendInitializer(AdminInvUtils.class);

        /**
         * EVENTS
         */

        event(new ShopInventoryEvents());
        event(new PlotListener());
        event(new TeleportEvent());
        event(new ChatEvent());
        event(new SignEvent());
        event(new TargetEvent());
        event(new ArmorStandCrashFix());
        event(new SpamKickEvent());
        event(new AdminInvCloseEvent());

        if (Main.version.equalsIgnoreCase("v1_8_R3")) {
            event(new ItemPickupLegacy());
        } else {
            event(new ItemPickup());
        }

        /**
         *COMMANDS
         */

        command(new AdminInvCommand(), "admininventory");
        command(new BackCommand(), "back");
        command(new BroadcastCommand(), "broadcast");
        command(new ClearCommand(), "clear");
        command(new DayCommand(), "day");
        command(new DelHomeCommand(), "delhome");
        command(new DelWarpCommand(), "delwarp");
        command(new EnderchestCommand(), "ec");
        command(new EcoCommand(), "eco");
        command(new EnchantCommand(), "enchant");
        command(new ForceChatCommand(), "fc");
        command(new FeedCommand(), "feed");
        command(new FireworkCommand(), "firework");
        command(new FlyCommand(), "fly");
        command(new GamemodeCommand(), "gamemode");
        command(new GodCommand(), "god");
        command(new HatCommand(), "hat");
        command(new HealCommand(), "heal");
        command(new HomeCommand(), "home");
        command(new HomeAdminCommand(), "homeadmin");
        command(new InvseeCommand(), "invsee");
        command(new ItemCommand(), "item");
        command(new ShopCommand(), "itemshop");
        command(new KickAllCommand(), "kickall");
        command(new KillCommand(), "kill");
        command(new LoreCommand(), "lore");
        command(new MoneyCommand(), "money");
        command(new NearCommand(), "near");
        command(new NightCommand(), "night");
        command(new PayCommand(), "pay");
        command(new PermShopCommand(), "pshop");
        command(new PTimeCommand(), "ptime");
        command(new RainCommand(), "rain");
        command(new RandomTeleportCommand(), "randomteleport");
        command(new RemoveLoreLineCommand(), "removeloreline");
        command(new RenameCommand(), "rename");
        command(new RepairCommand(), "repair");
        command(new SetHomeCommand(), "sethome");
        command(new SetLoreLineCommand(), "setloreline");
        command(new SetSpawnCommand(), "setspawn");
        command(new SetWarpCommand(), "setwarp");
        command(new SpawnCommand(), "spawn");
        command(new SpeedCommand(), "speed");
        command(new SudoCommand(), "sudo");
        command(new SunCommand(), "sun");
        command(new ToggleweatherCommand(), "toggleweather");
        command(new TpacceptCommand(), "tpaccept");
        command(new TpaCommand(), "tpa");
        command(new TpCommand(), "tp");
        command(new TpoCommand(), "tpo");
        command(new TpposCommand(), "tppos");
        command(new TptoggleCommand(), "tptoggle");
        command(new TrashCommand(), "trash");
        command(new UnchantCommand(), "unchant");
        command(new VanishCommand(), "vanish");
        command(new WarpCommand(), "warp");
        command(new WorkbenchCommand(), "workbench");

        /**
         * SEND COMPLETE STATUS
         */

        pluginState = PluginState.COMPLETE;

    }

    public void command(CommandExecutor executor, String command) {
        this.javaPlugin.getCommand(command).setExecutor(executor);
        Bukkit.getConsoleSender().sendMessage("§7[§cWPESS§7] §eInitialized command: " + command);
    }

    public void event(Listener listener) {
        this.javaPlugin.getServer().getPluginManager().registerEvents(listener, this.javaPlugin);
        Bukkit.getConsoleSender().sendMessage("§7[§cWPESS§7] §cInitialized listener: " + listener);
    }

    private void sendInitializer(Class<?> clazz) {

        Bukkit.getConsoleSender().sendMessage("§7[§cWPESS§7] §bInitialized necessary class: " + clazz);

    }

    public PluginState getPluginState() {
        return pluginState;
    }

    public static PluginState getState() {
        return pluginState;
    }

    public static void setState(PluginState state) {
        pluginState = state;
    }

}
