package net.woodpixel.essentials.antidupe;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

/*Still under construction, will be implemented later*/

public class DupeAPI {

    private static DupeAPI dupe;

    private List<World> worlds;

    public DupeAPI(JavaPlugin javaPlugin) {
        dupe = this;
        List<String> worldsString = javaPlugin.getConfig().getStringList("dupe.worlds");
        for (String s : worldsString) {

            if (Bukkit.getWorld(s) != null) {
                this.worlds.add(Bukkit.getWorld(s));
            }

        }
    }

    public static DupeAPI getDupe() {
        return dupe;
    }

    public List<Chunk> getLoadedChunks(World w) {

        List<Chunk> loaded = new ArrayList<>();

        loaded.addAll(Arrays.asList(w.getLoadedChunks()));

        return loaded;

    }

}
