package net.woodpixel.essentials;

import org.bukkit.entity.Player;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class PermissionUtil {

    private static PermissionUtil permissionUtil = new PermissionUtil();

    public PermissionUtil() {}

    public static PermissionUtil getUtil() {
        return permissionUtil;
    }

    /**
     * PERMISSIONS
     */

    public String PERM_TPA = "wpess.tpa";
    public String PERM_TPACCEPT = "wpess.tpaccept";
    public String PERM_TPO = "wpess.tpo";
    public String PERM_TP = "wpess.tp";
    public String PERM_TPTOGGLE = "wpess.tptoggle";
    public String PERM_TPPOS = "wpess.tppos";
    public String PERM_TPPOS_PER_WORLD = "wpess.tppos.world.";
    public String PERM_TPHERE = "wpess.tphere";
    public String PERM_TPOHERE = "wpess.tpohere";
    public String PERM_TPOHERE_PREVENT = "wpess.admin.tpohere.prevent";
    public String PERM_TPOHERE_BYPASS_PREVENT = "wpess.admin.tpohere.bypass.prevent";
    public String PERM_INVSEE = "wpess.invsee";
    public String PERM_INVSEE_MODIFY = "wpess.invsee.modify";
    public String PERM_INVSEE_MODIFY_PREVENT = "wpess.invsee.modify.prevent";
    public String PERM_INVSEE_ADMIN = "wpess.invsee.admin";
    public String PERM_ENDERCHEST = "wpess.ec";
    public String PERM_ENDERCHEST_MODIFY = "wpess.ec.modify";
    public String PERM_ENDERCHEST_MODIFY_PREVENT = "wpess.ec.modify.prevent";
    public String PERM_ENDERCHEST_ADMIN = "wpess.ec.admin";
    public String PERM_TOGGLEWEATHER = "wpess.toggleweather";
    public String PERM_FORCECHAT = "wpess.admin.forcechat";
    public String PERM_FORCECHAT_PREVENT = "wpess.admin.forcechat.prevent";
    public String PERM_SUDO = "wpess.admin.sudo";
    public String PERM_SUDO_PREVENT = "wpess.admin.sudo.prevent";
    public String PERM_GAMEMODE_BASE = "wpess.gamemode";
    public String PERM_GAMEMODE_SURVIVAL = "wpess.gamemode.survival";
    public String PERM_GAMEMODE_CREATIVE = "wpess.gamemode.creative";
    public String PERM_GAMEMODE_ADVENTURE = "wpess.gamemode.adventure";
    public String PERM_GAMEMODE_SPECTATOR = "wpess.gamemode.spectator";
    public String PERM_GAMEMODE_OTHER = "wpess.gamemode.other";
    public String PERM_GAMEMODE_PER_WORLD = "wpess.gamemode.world.";
    public String PERM_FLY = "wpess.fly";
    public String PERM_FLY_OTHER = "wpess.fly.other";
    public String PERM_HEAL = "wpess.heal";
    public String PERM_HEAL_OTHER = "wpess.heal.other";
    public String PERM_FEED = "wpess.feed";
    public String PERM_FEED_OTHER = "wpess.feed.other";
    public String PERM_PTIME = "wpess.ptime";
    public String PERM_VANISH = "wpess.vanish";
    public String PERM_VANISH_INTERACT = "wpess.vanish.interact";
    public String PERM_VANISH_PVP = "wpess.vanish.pvp";
    public String PERM_VANISH_OTHER = "wpess.vanish.other";
    public String PERM_VANISH_SEE_TEAM = "wpess.vanish.see.team";
    public String PERM_VANISH_SEE_ALL = "wpess.vanish.see.all";
    public String PERM_VANISH_ONJOIN_TEAM = "wpess.vanish.onjoin.team";
    public String PERM_VANISH_ONJOIN_ADMIN = "wpess.vanish.onjoin.admin";
    public String PERM_GOD = "wpess.god";
    public String PERM_GOD_OTHER = "wpess.god.other";
    public String PERM_ITEM = "wpess.item";
    public String PERM_KEEP_OPTIONS = "wpess.keepoptions";
    public String PERM_DAY = "wpess.day";
    public String PERM_NIGHT = "wpess.night";
    public String PERM_ECO = "wpess.eco";
    public String PERM_MONEY = "wpess.money";
    public String PERM_MONEY_OTHER = "wpess.money.other";
    public String PERM_PAY = "wpess.pay";
    public String PERM_PAY_SAMEIP = "wpess.pay.sameip";
    public String PERM_PAY_ALL = "wpess.pay.all";
    public String PERM_PAY_CREDIT = "wpess.pay.credit";
    public String PERM_SPAWN = "wpess.spawn";
    public String PERM_SET_SPAWN = "wpess.setspawn";
    public String PERM_REPAIR = "wpess.repair";
    public String PERM_ENCHANT = "wpess.enchant";
    public String PERM_WARP = "wpess.warp";
    public String PERM_DELWARP = "wpess.delwarp";
    public String PERM_SETWARP = "wpess.setwarp";
    public String PERM_HAT = "wpess.hat";
    public String PERM_SPEED = "wpess.speed";
    public String PERM_SPEED_OTHER = "wpess.speed.other";
    public String PERM_HOME = "wpess.home";
    public String PERM_SETHOME = "wpess.sethome";
    public String PERM_DELHOME = "wpess.delhome";
    public String PERM_HOMEADMIN = "wpess.homeadmin";
    public String PERM_NEAR = "wpess.near";
    public String PERM_NEAR_VANISHED = "wpess.near.vanished";
    public String PERM_RAIN = "wpess.rain";
    public String PERM_SUN = "wpess.sun";
    public String PERM_KILL = "wpess.kill";
    public String PERM_CLEAR = "wpess.clear";
    public String PERM_CLEAR_OTHER = "wpess.clear.other";
    public String PERM_BROADCAST = "wpess.broadcast";
    public String PERM_RANDOM_TELEPORT = "wpess.randomtp";
    public String PERM_KICK_ALL = "wpess.kickall";
    public String PERM_CHEST_CONTROL = "wpess.chestcontrol";
    public String PERM_CHEST_CONTROL_BYPASS = "wpess.chestcontrol.bypass";
    public String PERM_WORKBENCH = "wpess.workbench";
    public String PERM_BACK = "wpess.back";
    public String PERM_BACK_OTHER = "wpess.back.other";
    public String PERM_PACK_FONT_COLOR = "wpess.font.color.all";
    public String PERM_PACK_FONT_FORMAT = "wpess.font.format.all";
    public String PERM_FONT_COLOR_BASE = "wpess.font.color.";
    public String PERM_FONT_FORMAT_BASE = "wpess.font.format.";
    public String PERM_SHOP_ADMIN = "wpess.shop.admin";
    public String PERM_KEEP_ON_WORLD_CHANGE = "wpess.worldchange.keep";
    public String PERM_BYPASS_SPAM_DISCONNECT = "wpess.spam.bypass.disconnect";
    public String PERM_FIREWORK = "wpess.firework";
    public String PERM_FIREWORK_BYPASS_LIMIT = "wpess.firework.bypass.limit";
    public String PERM_UNCHANT = "wpess.unchant";
    public String PERM_ADMIN_INV = "wpess.admininv";
    public String PERM_ADMIN_INV_OTHER = "wpess.admininv.other";
    public String PERM_RENAME = "wpess.rename";
    public String PERM_LORE = "wpess.lore";
    public String PERM_SET_LORE_LINE = "wpess.setlore";
    public String PERM_REMOVE_LORE_LINE = "wpess.removelore";
    public String PERM_TRASH = "wpess.trash";

    /**
     * PUBLIC METHODS
     */


    public boolean hasPermission(Player p, String permission) {
        return p.hasPermission(permission);
    }

    public void returnNoPermission(Player p, String permission) {
        p.sendMessage(Data.getData().prefix + "§cDir fehlt die folgende Berechtigung: §6" + permission);
    }


    /**
     * PRIVATE METHODS
     */

}
