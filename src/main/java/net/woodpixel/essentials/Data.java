package net.woodpixel.essentials;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class Data {

    private static Data data;

    public Data() {
        data = this;
    }

    public static Data getData() {
        return data;
    }

    /**
     * DATA VALUES
     */

    public String prefix;
    public String noPerm;

    public void returnNoConsoleCommand(CommandSender sender, Command command) {
        sender.sendMessage(prefix + "§cDu musst ein Spieler sein um den Command §6/" + command.getLabel() + " §causzuführen!");
    }

    public void consoleIssuedNotify(String message) {
        Bukkit.broadcast("§7§o[Console] " + message, "wpess.console.message");
    }

}
