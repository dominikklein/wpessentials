package net.woodpixel.essentials.commandUtils;

import net.woodpixel.essentials.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class TeleportUtil {

    private static TeleportUtil teleportUtil;
    private JavaPlugin javaPlugin;

    public TeleportUtil(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
    }

    public void setTeleportUtil() {
        teleportUtil = this;
    }

    public static TeleportUtil getTeleportUtil() {
        return teleportUtil;
    }

    private HashMap<Player, Player> tpaHashSets = new HashMap<Player, Player>();
    private HashMap<Player, Player> tpaHashSetsReverse = new HashMap<>();
    private HashMap<Player, Long> tpaTimeOut = new HashMap<>();


    public void sendTpa(Player p, Player target) {

        if (tpaHashSetsReverse.containsKey(p)) {
            if (tpaHashSetsReverse.get(p).equals(target)) {
                p.sendMessage(Data.getData().prefix + "§cDu hast diesem Spieler bereits eine Anfrage gesendet!");
                return;
            }
        }

        sendTpaMessage(p, target);
        tpaHashSets.put(target, p);
        tpaHashSetsReverse.put(p, target);

        new BukkitRunnable() {

            @Override
            public void run() {

                tpaHashSets.remove(target, p);
                tpaHashSetsReverse.remove(p, target);

            }

        }.runTaskLaterAsynchronously(this.javaPlugin, 1200L);

    }

    public void acceptTpa(Player p) {

        tpaHashSets.get(p).teleport(p.getLocation());
        p.sendMessage(Data.getData().prefix + "§aDu hast die Teleport-Anfrage angenommen!");
        tpaHashSets.get(p).sendMessage(Data.getData().prefix + p.getDisplayName() + " §ahat die Teleport-Anfrage angenommen!");
        tpaHashSetsReverse.remove(tpaHashSets.get(p));
        tpaHashSets.remove(p);

    }

    public boolean hasTpa(Player p) {

        return tpaHashSets.containsKey(p);

    }

    public void removeOnQuit(Player p) {

        for (Player pl : Bukkit.getOnlinePlayers()) {

            if (tpaHashSets.get(pl) != null) {
                if (tpaHashSets.get(pl).equals(p)) {
                    tpaHashSets.remove(pl);
                }
            }

        }

        if (tpaHashSetsReverse.containsKey(p)) {
            tpaHashSetsReverse.remove(p);
        }

    }

    public boolean isWorldPermitted(String world, Player player, String permissionBase) {

        return player.hasPermission(permissionBase + world);

    }

    /**
     * PRIVATE METHODS
     */

    private void sendTpaMessage(Player p, Player target) {

        p.sendMessage(Data.getData().prefix + "§6Du hast " + target.getDisplayName() + " §6eine TPA-Anfrage gesendet!");
        target.sendMessage(Data.getData().prefix + "§6Du hast eine TPA-Anfrage von " + p.getDisplayName() +
                " §6bekommen! Gebe §b/tpaccept §6ein um diese Anfrage anzunehmen!");

    }

}
