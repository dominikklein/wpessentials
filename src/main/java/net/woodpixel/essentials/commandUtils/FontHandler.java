package net.woodpixel.essentials.commandUtils;

import net.woodpixel.essentials.PermissionUtil;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class FontHandler {

    private PermissionUtil permissionUtil = PermissionUtil.getUtil();
    private static FontHandler fontHandler;
    private List<String> charsColor = new ArrayList<>();
    private List<String> charsFormat = new ArrayList<>();

    public FontHandler() {
        fontHandler = this;
        charsColor.add("a");
        charsColor.add("b");
        charsColor.add("c");
        charsColor.add("d");
        charsColor.add("e");
        charsColor.add("f");
        charsColor.add("A");
        charsColor.add("B");
        charsColor.add("C");
        charsColor.add("D");
        charsColor.add("E");
        charsColor.add("F");
        charsColor.add("0");
        charsColor.add("1");
        charsColor.add("2");
        charsColor.add("3");
        charsColor.add("4");
        charsColor.add("5");
        charsColor.add("6");
        charsColor.add("7");
        charsColor.add("8");
        charsColor.add("9");
        charsFormat.add("l");
        charsFormat.add("n");
        charsFormat.add("o");
        charsFormat.add("k");
        charsFormat.add("m");
        charsFormat.add("r");
        charsFormat.add("L");
        charsFormat.add("N");
        charsFormat.add("O");
        charsFormat.add("K");
        charsFormat.add("M");
        charsFormat.add("R");
    }

    public static FontHandler getFontHandler() {
        return fontHandler;
    }

    public boolean isCharPermitted(String chars1, Player player) {

        String c = chars1.substring(1);

        if (charsColor.contains(c)) {
            if (permissionUtil.hasPermission(player, permissionUtil.PERM_FONT_COLOR_BASE + c)) {
                return true;
            }
            else if (permissionUtil.hasPermission(player, permissionUtil.PERM_PACK_FONT_COLOR)) {
                return true;
            }
            else {
                return false;
            }
        }

        else if (charsFormat.contains(c)) {
            if (permissionUtil.hasPermission(player, permissionUtil.PERM_FONT_FORMAT_BASE + c)) {
                return true;
            }
            else if (permissionUtil.hasPermission(player, permissionUtil.PERM_PACK_FONT_FORMAT)) {
                return true;
            }
            else {
                return false;
            }
        }

        else {
            return false;
        }

    }

    public String calculate(String input, Player player) {

        char[] c = input.toCharArray();
        List<Integer> position = new ArrayList<>();

        int i = 0;

        for (Character character : c) {
            if (character.equals("&".toCharArray()[0])) {
                position.add(i);
            }
            i++;
        }

        String s;
        StringBuilder sb = new StringBuilder(input);
        for (int ii : position) {
            String cChar = String.valueOf(input.charAt(ii + 1));
            if (isCharPermitted("&" + cChar, player)) {
                char[] cc = "§".toCharArray();
                sb.setCharAt(ii, cc[0]);
            }
        }
        s = sb.toString();

        return s;

    }

}
