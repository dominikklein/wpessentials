package net.woodpixel.essentials.commandUtils;

import net.woodpixel.essentials.Main;
import net.woodpixel.essentials.configuration.PlayerDataInstance;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class AdminInvUtils {

    private static AdminInvUtils utils;
    private JavaPlugin javaPlugin;


    public AdminInvUtils(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        utils = this;
    }

    public static AdminInvUtils getUtils() {
        return utils;
    }

    public void createPlayerFile(OfflinePlayer p) {

        File file = new File(javaPlugin.getDataFolder().getPath() + "//inventorys//", p.getUniqueId().toString() + ".yml");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("[DEBUG] User file coudn't be created!");
            }
        }

    }

    public boolean existsPlayer(OfflinePlayer p) {

        File file = new File(javaPlugin.getDataFolder().getPath() + "//inventorys//", p.getUniqueId().toString() + ".yml");
        return file.exists();

    }

    public void saveInventory(Inventory inventory, int page, OfflinePlayer p) {

        File file = new File(javaPlugin.getDataFolder().getPath() + "//inventorys//", p.getUniqueId().toString() + ".yml");
        YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);

        configuration.set("inv.items.page" + page, null);

        for (int i = 0; i < 54; i++) {

            if (inventory.getItem(i) != null) {
                configuration.set("inv.items.page" + page + ".itemslot" + i, inventory.getItem(i));
            }

        }

        try {
            configuration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void loadInventory(OfflinePlayer p, Player player, int page) {

        createPlayerFile(p);

        File file = new File(javaPlugin.getDataFolder().getPath() + "//inventorys//", p.getUniqueId().toString() + ".yml");
        YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        try {
            configuration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

            Inventory inventory = Bukkit.createInventory(null, 54, "§5" + p.getName() + "-" + page);

            ItemStack back = new ItemStack(Material.PAPER);
            ItemMeta iBack = back.getItemMeta();
            iBack.setDisplayName("§cZurück");
            back.setItemMeta(iBack);

            ItemStack forward = new ItemStack(Material.PAPER);
            ItemMeta iForward = back.getItemMeta();
            iForward.setDisplayName("§aWeiter");
            forward.setItemMeta(iForward);

            inventory.setItem(53, forward);
            inventory.setItem(45, back);

            for (int ii = 0; ii < 54; ii++) {
                if (configuration.getItemStack("inv.items.page" + page + ".itemslot" + ii) != null) {
                    ItemStack is = configuration.getItemStack("inv.items.page" + page + ".itemslot" + ii);
                    inventory.addItem(is);
                }
            }

            inventory.setItem(53, forward);
            inventory.setItem(45, back);

            player.openInventory(inventory);

    }

}
