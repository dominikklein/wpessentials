package net.woodpixel.essentials.commandUtils;

import org.bukkit.Material;

import java.util.HashMap;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ItemAdaptor {

    private HashMap<String, Material> materialMapping = new HashMap<>();
    private static ItemAdaptor itemAdaptor;

    public ItemAdaptor() {
        init();
        itemAdaptor = this;
    }

    public static ItemAdaptor getAdaptor() {
        return itemAdaptor;
    }

    private void insert(String input, Material output) {
        materialMapping.put(input, output);
    }

    private boolean containsRequested(String input) {
        return materialMapping.containsKey(input.toLowerCase());
    }

    public Material getRequest(String request) {
        if (containsRequested(request)) {
            return materialMapping.get(request);
        }
        return null;
    }

    private void init() {
        insert("spawner", Material.MOB_SPAWNER);
        insert("mobspawner", Material.MOB_SPAWNER);
        insert("dragonegg", Material.DRAGON_EGG);
        insert("steak", Material.COOKED_BEEF);
        insert("cobble", Material.COBBLESTONE);
        insert("goldore", Material.GOLD_ORE);
        insert("ironore", Material.IRON_ORE);
        insert("coalore", Material.COAL_ORE);
        insert("diamondore", Material.DIAMOND_ORE);
        insert("emeraldore", Material.EMERALD_ORE);
        insert("redstoneore", Material.REDSTONE_ORE);
        insert("lapisore", Material.LAPIS_ORE);
        insert("goldblock", Material.GOLD_BLOCK);
        insert("ironblock", Material.IRON_BLOCK);
        insert("coalblock", Material.COAL_BLOCK);
        insert("diamondblock", Material.DIAMOND_BLOCK);
        insert("emeraldblock", Material.EMERALD_BLOCK);
        insert("lapisblock", Material.LAPIS_BLOCK);
        insert("redstoneblock", Material.REDSTONE_BLOCK);
        insert("poweredrail", Material.POWERED_RAIL);
        insert("detectorrail", Material.DETECTOR_RAIL);
        insert("stickypiston", Material.PISTON_STICKY_BASE);
        insert("yellowflower", Material.YELLOW_FLOWER);
        insert("stoneslab", Material.STONE_SLAB2);
        insert("oakstairs", Material.DARK_OAK_STAIRS);
        insert("craftingtable", Material.WORKBENCH);
        insert("workbench", Material.WORKBENCH);
        insert("woodendoor", Material.WOODEN_DOOR);
        insert("irondoor", Material.IRON_DOOR);
        insert("redstonetorch", Material.REDSTONE_TORCH_OFF);
        insert("stonebutton", Material.STONE_BUTTON);
        insert("woodbutton", Material.WOOD_BUTTON);
        insert("soulsand", Material.SOUL_SAND);
        insert("stainedglass", Material.STAINED_GLASS);
        insert("stainedglasspane", Material.STAINED_GLASS_PANE);
        insert("irontrapdoor", Material.IRON_TRAPDOOR);
        insert("woodentrapdoor", Material.TRAP_DOOR);
        insert("melonblock", Material.MELON_BLOCK);
        insert("torch", Material.TORCH);
    }

}
