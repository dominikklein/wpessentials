package net.woodpixel.essentials.commandUtils;

import com.intellectualcrafters.plot.api.PlotAPI;
import com.intellectualcrafters.plot.object.Location;
import com.intellectualcrafters.plot.object.Plot;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;

import java.util.ArrayList;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

/*further implementation of plot chests will start later*/

public class PlotChestUtil {

    private final Plot plot;
    private PlotAPI api = new PlotAPI();

    public PlotChestUtil(Plot plot) {
        this.plot = plot;
    }

    public ArrayList<Block> getBlocks() {

        Location[] locations = this.plot.getCorners();

        ArrayList<Block> blocks = new ArrayList<>();

        int x0 = locations[0].getX();
        int x1 = locations[1].getX();

        int y0 = locations[0].getY();
        int y1 = locations[1].getY();

        int z0 = locations[0].getZ();
        int z1 = locations[1].getZ();

        World w = Bukkit.getWorld(locations[0].getWorld());

        for (int i = x0; i < x1; i++) {

            for (int j = y0; j < y1; j++) {

                for (int k = z0; k < z1; k++) {

                    blocks.add(w.getBlockAt(i, j, k));

                }

            }

        }

        return blocks;

    }

    public ArrayList<Block> getChests() {

        ArrayList<Block> blocks = new ArrayList<>();

        for (Block block : getBlocks()) {
            if (block instanceof Chest) {
                blocks.add(block);
            }
            if (block instanceof DoubleChest) {
                blocks.add(block);
            }
        }

        return blocks;

    }



}
