package net.woodpixel.essentials.commandUtils;

import net.woodpixel.essentials.Data;
import net.woodpixel.essentials.PermissionUtil;
import net.woodpixel.essentials.event.VanishEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class VanishHelper implements Listener {

    private static VanishHelper vanishHelper;
    PermissionUtil permUtil = PermissionUtil.getUtil();
    private JavaPlugin javaPlugin;

    private static HashMap<Player, String> vanishMapping = new HashMap<>();

    public static VanishHelper getVanishHelper() {
        return vanishHelper;
    }

    public VanishHelper(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        vanishHelper = this;
        this.javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    public boolean isVanished(Player p) {

        if (vanishMapping.get(p) != null) {
            return true;
        }

        else {
            return false;
        }

    }

    public void vanishPlayer(Player p, String value) {

        for (Player all : Bukkit.getOnlinePlayers()) {

            if (permUtil.hasPermission(all, permUtil.PERM_VANISH_SEE_ALL)) {
                vanishMapping.put(p, value);
            }

            else {
                all.hidePlayer(p);
            }

        }

        vanishMapping.put(p, value);

        for (Player players : vanishMapping.keySet()) {

            if (vanishMapping.get(players).equalsIgnoreCase("team")) {

                p.showPlayer(players);

            }

        }

    }

    public void unvanishPlayer(Player p) {

        for (Player all : Bukkit.getOnlinePlayers()) {

            all.showPlayer(p);

        }

        vanishMapping.remove(p);

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        if (permUtil.hasPermission(p, permUtil.PERM_VANISH_ONJOIN_ADMIN)) {

            vanishPlayer(p, "admin");
            p.sendMessage(Data.getData().prefix + "§aDu hast den Server im Vanish betreten!");
            this.javaPlugin.getServer().getPluginManager().callEvent(new VanishEvent(p, VanishEvent.VanishStates.VANISH));
            vanishMapping.put(p, "admin");

        }

        if (permUtil.hasPermission(p, permUtil.PERM_VANISH_ONJOIN_TEAM) && !permUtil.hasPermission(p, permUtil.PERM_VANISH_ONJOIN_ADMIN)) {

            vanishPlayer(p, "team");
            p.sendMessage(Data.getData().prefix + "§aDu hast den Server im Vanish betreten!");
            this.javaPlugin.getServer().getPluginManager().callEvent(new VanishEvent(p, VanishEvent.VanishStates.VANISH));

        }

        if (permUtil.hasPermission(p, permUtil.PERM_VANISH_SEE_ALL)) {

            for (Player all : Bukkit.getOnlinePlayers()) {

                p.showPlayer(all);

            }

            return;

        }

        for (Player players : vanishMapping.keySet()) {

            p.hidePlayer(players);

        }

        if (permUtil.hasPermission(p, permUtil.PERM_VANISH_SEE_TEAM)) {

            for (Player players : vanishMapping.keySet()) {

                if (vanishMapping.get(players).equalsIgnoreCase("team")) {

                    p.showPlayer(players);
                    players.showPlayer(p);

                }

            }

        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();

        if (isVanished(p)) {

            vanishMapping.remove(p);

        }

    }

}
