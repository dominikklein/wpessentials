package net.woodpixel.essentials.commandUtils;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;

import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public interface IHomeManager {

    void setHome(OfflinePlayer p, Location location, String name);

    void delHome(OfflinePlayer p, String name);

    List<String> listHomes(OfflinePlayer p);

    Location getHome(OfflinePlayer p, String name);

    boolean exists(OfflinePlayer p, String name);

}
