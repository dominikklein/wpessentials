package net.woodpixel.essentials.commandUtils;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class ConsoleWorker implements IConsoleWorker {

    @Override
    public boolean isConsoleSender(CommandSender sender) {

        if (sender instanceof ConsoleCommandSender) {
            return true;
        }

        return false;
    }

    @Override
    public void executeConsoleCommand(String consoleCommand) {
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), consoleCommand);
    }
}
