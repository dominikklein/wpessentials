package net.woodpixel.essentials.commandUtils;

import net.woodpixel.essentials.configuration.PlayerDataInstance;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class HomeManager implements IHomeManager {

    private static HomeManager homeManager;

    public HomeManager() {
        homeManager = this;
    }

    public static HomeManager getHomeManager() {
        return homeManager;
    }

    @Override
    public void setHome(OfflinePlayer p, Location location, String name) {

        name = name.toLowerCase();

        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);

        List<String> homes = configuration.getStringList("homeList");
        homes.add(name);

        configuration.set("home." + name + ".x", location.getX());
        configuration.set("home." + name + ".y", location.getY());
        configuration.set("home." + name + ".z", location.getZ());
        configuration.set("home." + name + ".yaw", location.getYaw());
        configuration.set("home." + name + ".pitch", location.getPitch());
        configuration.set("home." + name + ".w", location.getWorld().getName());
        configuration.set("homeList", homes);
        try {
            configuration.save(PlayerDataInstance.getpDI().getPlayerFile(p));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delHome(OfflinePlayer p, String name) {

        name = name.toLowerCase();

        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);

        List<String> homes = configuration.getStringList("homeList");
        homes.remove(name);

        configuration.set("home." + name, null);
        configuration.set("homeList", homes);
        try {
            configuration.save(PlayerDataInstance.getpDI().getPlayerFile(p));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<String> listHomes(OfflinePlayer p) {

        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);
        if (configuration.get("homeList") == null) {
            List<String> test = new ArrayList<>();
            return test;
        }
        List<String> homes = configuration.getStringList("homeList");
        return homes;

    }

    @Override
    public Location getHome(OfflinePlayer p, String name) {

        name = name.toLowerCase();

        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);

        double x = configuration.getDouble("home." + name + ".x");
        double y = configuration.getDouble("home." + name + ".y");
        double z = configuration.getDouble("home." + name + ".z");
        float yaw = configuration.getInt("home." + name + ".yaw");
        float pitch = configuration.getInt("home." + name + ".pitch");
        World w = Bukkit.getWorld(configuration.getString("home." + name + ".w"));

        return new Location(w, x, y, z, yaw, pitch);

    }

    @Override
    public boolean exists(OfflinePlayer p, String name) {

        name = name.toLowerCase();

        YamlConfiguration configuration = PlayerDataInstance.getpDI().getPlayerConfig(p);

        if (configuration.get("home." + name) == null) {
            return false;
        }
        return true;

    }

}
