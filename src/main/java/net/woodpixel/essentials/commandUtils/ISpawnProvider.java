package net.woodpixel.essentials.commandUtils;

import org.bukkit.Location;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public interface ISpawnProvider {

    /**
     * @return Spawn Location
     */
    Location getSpawnLocation();

    /**
     * LOAD LOCAL SPAWN
     */
    void loadSpawnLocation();

    /**
     * DEFINE SPAWN
     * @param location
     */
    void setSpawnLocation(Location location);

}
