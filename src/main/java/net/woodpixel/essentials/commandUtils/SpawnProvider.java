package net.woodpixel.essentials.commandUtils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class SpawnProvider implements ISpawnProvider {

    private static SpawnProvider spawnProvider;
    private JavaPlugin javaPlugin;
    private Location spawn = null;

    public SpawnProvider(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        spawnProvider = this;
    }

    public static SpawnProvider getSpawnProvider() {
        return spawnProvider;
    }

    /**
     * INTERFACE METHODS
     */

    @Override
    public void loadSpawnLocation() {

        try {

            World w = Bukkit.getWorld(this.javaPlugin.getConfig().getString("spawn.world"));
            double x = this.javaPlugin.getConfig().getDouble("spawn.x");
            double y = this.javaPlugin.getConfig().getDouble("spawn.y");
            double z = this.javaPlugin.getConfig().getDouble("spawn.z");
            float yaw = this.javaPlugin.getConfig().getInt("spawn.yaw");
            float pitch = this.javaPlugin.getConfig().getInt("spawn.pitch");

            this.spawn = new Location(w, x, y, z, yaw, pitch);
            /**System.out.println("SPAWN: " + x + ", " + y + ", " + z + ", " + yaw + ", " + pitch + ", " + w);**/

        } catch (NullPointerException | IllegalArgumentException e) {
            System.out.println("[INFO][WPESS] DER SPAWN WURDE NOCH NICHT GESETZT!");
            this.spawn = null;
        }

    }

    @Override
    public Location getSpawnLocation() {
        return this.spawn;
    }

    @Override
    public void setSpawnLocation(Location location) {

        this.javaPlugin.getConfig().set("spawn.world", location.getWorld().getName());
        this.javaPlugin.getConfig().set("spawn.x", location.getX());
        this.javaPlugin.getConfig().set("spawn.y", location.getY());
        this.javaPlugin.getConfig().set("spawn.z", location.getZ());
        this.javaPlugin.getConfig().set("spawn.yaw", location.getYaw());
        this.javaPlugin.getConfig().set("spawn.pitch", location.getPitch());
        this.javaPlugin.saveConfig();

        loadSpawnLocation();

    }
}
