package net.woodpixel.essentials.commandUtils;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class InventoryHelper implements Listener {

    private List<Player> blockMove = new ArrayList<Player>();
    private static InventoryHelper invseeHelper;

    public void setInventory() {
        invseeHelper = this;
    }

    public InventoryHelper(JavaPlugin javaPlugin) {
        javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    public static InventoryHelper getHelper() {
        return invseeHelper;
    }

    public void cancelInventoryActions(Player p) {
        blockMove.add(p);
    }

    public void resumeInventoryAction(Player p) {
        blockMove.remove(p);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onClick(InventoryClickEvent e) {

        Player p = (Player)e.getWhoClicked();

        if (blockMove.contains(p)) {
            e.setCancelled(true);
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onDrag(InventoryDragEvent e) {

        Player p = (Player)e.getWhoClicked();

        if (blockMove.contains(p)) {
            e.setCancelled(true);

        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onInventoryClose(InventoryCloseEvent e) {

        if (blockMove.contains(e.getPlayer())) {
            resumeInventoryAction((Player)e.getPlayer());
        }

    }

}
