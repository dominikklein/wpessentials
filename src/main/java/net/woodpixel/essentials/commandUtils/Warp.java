package net.woodpixel.essentials.commandUtils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class Warp implements IWarp {

    private JavaPlugin javaPlugin;
    private static Warp warp;

    public Warp(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        warp = this;
    }

    public static Warp getWarp() {
        return warp;
    }

    @Override
    public void createWarp(String name, Location location) {

        name = name.toLowerCase();

        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        float yaw = location.getYaw();
        float pitch = location.getPitch();
        String worldName = location.getWorld().getName();

        List<String> warps = this.javaPlugin.getConfig().getStringList("warpList");
        warps.add(name);

        this.javaPlugin.getConfig().set("warps." + name + ".x", x);
        this.javaPlugin.getConfig().set("warps." + name + ".y", y);
        this.javaPlugin.getConfig().set("warps." + name + ".z", z);
        this.javaPlugin.getConfig().set("warps." + name + ".yaw", yaw);
        this.javaPlugin.getConfig().set("warps." + name + ".pitch", pitch);
        this.javaPlugin.getConfig().set("warps." + name + ".world", worldName);
        this.javaPlugin.getConfig().set("warpList", warps);
        this.javaPlugin.saveConfig();

    }

    @Override
    public void deleteWarp(String name) {

        name = name.toLowerCase();

        List<String> warps = this.javaPlugin.getConfig().getStringList("warpList");
        warps.remove(name);

        this.javaPlugin.getConfig().set("warps." + name, null);
        this.javaPlugin.getConfig().set("warpList", warps);
        this.javaPlugin.saveConfig();

    }

    @Override
    public boolean exists(String name) {

        name = name.toLowerCase();

        if (this.javaPlugin.getConfig().getString("warps." + name) == null) {
            return false;
        }
        return true;

    }

    @Override
    public boolean existsWorld(String name) {

        name = name.toLowerCase();

        if (Bukkit.getWorld(this.javaPlugin.getConfig().getString("warps." + name + ".world")) != null) {
            return true;
        }
        return false;

    }

    @Override
    public Location getWarpLocation(String name) {

        name = name.toLowerCase();

        double x = this.javaPlugin.getConfig().getDouble("warps." + name + ".x");
        double y = this.javaPlugin.getConfig().getDouble("warps." + name + ".y");
        double z = this.javaPlugin.getConfig().getDouble("warps." + name + ".z");
        float yaw = this.javaPlugin.getConfig().getInt("warps." + name + ".yaw");
        float pitch = this.javaPlugin.getConfig().getInt("warps." + name + ".pitch");
        World w = Bukkit.getWorld(this.javaPlugin.getConfig().getString("warps." + name + ".world"));

        return new Location(w, x, y, z, yaw, pitch);

    }

    @Override
    public List<String> getWarps() {
        return this.javaPlugin.getConfig().getStringList("warpList");
    }
}
