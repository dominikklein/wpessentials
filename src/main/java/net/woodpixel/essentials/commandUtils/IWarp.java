package net.woodpixel.essentials.commandUtils;

import org.bukkit.Location;

import java.util.List;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public interface IWarp {

    void createWarp(String name, Location location);

    void deleteWarp(String name);

    boolean exists(String name);

    boolean existsWorld(String name);

    Location getWarpLocation(String name);

    List<String> getWarps();

}
