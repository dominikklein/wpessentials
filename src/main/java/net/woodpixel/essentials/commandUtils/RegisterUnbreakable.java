package net.woodpixel.essentials.commandUtils;

import org.bukkit.enchantments.Enchantment;

import java.lang.reflect.Field;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public class RegisterUnbreakable {

    public RegisterUnbreakable() {
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Unbreakable unbreakable = new Unbreakable(111);
            Glow glow = new Glow(112);
            Enchantment.registerEnchantment(unbreakable);
            Enchantment.registerEnchantment(glow);
        }
        catch (IllegalArgumentException e){
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }

}
