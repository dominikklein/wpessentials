package net.woodpixel.essentials.commandUtils;

import org.bukkit.command.CommandSender;

/**
 * Copyright (C) WoodPixel.NET - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dominik Klein <dominik.klein@woodpixel.net>, 2019
 **/

public interface IConsoleWorker {

    boolean isConsoleSender(CommandSender sender);

    void executeConsoleCommand(String consoleCommand);

}
